import { SET_SELECTED_TAB } from "../actions/ActionTypes";

export function setSelectedTab(selectedIndex) {
  return {
    selectedIndex,
    type: SET_SELECTED_TAB
  };
}
