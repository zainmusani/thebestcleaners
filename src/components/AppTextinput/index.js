// @flow
import _ from "lodash";
import React from "react";
import PropTypes from "prop-types";
import {
  TextInput as RNTextInput,
  ViewPropTypes,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Text, ButtonView } from "../";
import { Colors, AppStyles, Images } from "../../theme";
import { TextField } from "react-native-material-textfield";
import { Actions } from "react-native-router-flux";

export default class AppTextInput extends React.PureComponent {
  static propTypes = {
    value: PropTypes.string.isRequired,
    label: PropTypes.string,
    containerStyle: ViewPropTypes.style,
    onPress: PropTypes.func,
    multiline: PropTypes.bool,
    icon: PropTypes.number.isRequired,
    onSubmitEditing: PropTypes.func,
    onChangeText: PropTypes.func,
    secureTextEntry: PropTypes.bool,
    keyboardType: PropTypes.string
  };

  static defaultProps = {
    label: "",
    containerStyle: {},
    onPress: null,
    multiline: false,
    onSubmitEditing: () => {
      console.log("onsubmit");
    },
    onChangeText: () => {
      console.log("onChangeText");
    },
    secureTextEntry: false,
    keyboardType: "default"
  };

  focus() {
    this.myRef.focus();
  }

  blur() {
    this.myRef.blur();
  }

  render() {
    const {
      label,
      containerStyle,
      onPress,
      multiline,
      icon,
      onSubmitEditing,
      onChangeText,
      secureTextEntry,
      keyboardType,
      ref,
      value,
      ...rest
    } = this.props;
    if (label === "Location") {
      return (
        <TouchableOpacity
          style={containerStyle}
          onPress={() => {
            Actions.location();
          }}
        >
          <Image source={icon} style={{ position: "absolute", bottom: 24 }} />
          <Image
            source={Images.arrow_right}
            style={{ position: "absolute", bottom: 24, right: 2 }}
          />

          <View
            style={{
              paddingLeft: 30,
              borderBottomColor: "black",
              borderBottomWidth: 0.5
            }}
          >
            <TextField
              value={value}
              label={label}
              labelPadding={5}
              disabled={true}
              disabledLineWidth={0}
              inputContainerPadding={15}
              lineWidth={0}
              ref={ref => (this.myRef = ref)}
              activeLineWidth={0}
              baseColor={Colors.text.primary}
              textColor={Colors.text.primary}
              tintColor={Colors.text.orange}
              onSubmitEditing={onSubmitEditing}
              onChangeText={onChangeText}
              keyboardType={keyboardType}
              titleTextStyle={{
                fontFamily: "CirceRounded-Regular"
              }}
              labelTextStyle={{
                fontFamily: "CirceRounded-Regular",
                lineHeight: 20
              }}
              style={{ fontFamily: "CirceRounded-Regular", lineHeight: 50 }}
            />
          </View>
        </TouchableOpacity>
      );
    }
    return (
      <View style={containerStyle}>
        <Image source={icon} style={{ position: "absolute", bottom: 24 }} />
        <View
          style={{
            paddingLeft: 30,
            borderBottomColor: "black",
            borderBottomWidth: 0.5
          }}
        >
          <TextField
            label={label}
            labelPadding={5}
            inputContainerPadding={15}
            lineWidth={0}
            activeLineWidth={0}
            ref={ref => (this.myRef = ref)}
            baseColor={Colors.text.primary}
            textColor={Colors.text.primary}
            tintColor={Colors.text.orange}
            onSubmitEditing={onSubmitEditing}
            onChangeText={onChangeText}
            secureTextEntry={secureTextEntry}
            keyboardType={keyboardType}
            titleTextStyle={{
              fontFamily: "CirceRounded-Regular"
            }}
            labelTextStyle={{
              fontFamily: "CirceRounded-Regular",
              lineHeight: 20
            }}
            style={{ fontFamily: "CirceRounded-Regular", lineHeight: 20 }}
          />
        </View>
      </View>
    );
  }
}
