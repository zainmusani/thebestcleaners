// @flow
import React from "react";
import PropTypes from "prop-types";
import { View, Image, TouchableOpacity } from "react-native";
import { Text } from "../";
import styles from "./styles";
import { Colors, Images, AppStyles } from "../../theme";
import { appActiveOpacity } from "../../constants";

export default class OrderItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      w_i_count: 0,
      i_count: 0,
      total: 0
    };
  }
  static propTypes = {
    item: PropTypes.object.isRequired,
    showCheckout: PropTypes.func.isRequired
  };

  static defaultProps = {};

  _w_i_addClick() {
    const { w_i_count, total } = this.state;

    let item_count = w_i_count;
    let totalAmount = total;

    item_count += 1;
    totalAmount += parseInt(this.props.item.price_was_iron);
    this.setState({ w_i_count: item_count, total: totalAmount }, () => {
      if (this.state.total > 0) {
        this.props.showCheckout(true);
      } else {
        this.props.showCheckout(false);
      }
    });
  }

  _w_i_subClick() {
    const { w_i_count, total } = this.state;

    let item_count = w_i_count;
    let totalAmount = total;
    if (item_count > 0) {
      item_count -= 1;
      totalAmount -= parseInt(this.props.item.price_was_iron);
      this.setState({ w_i_count: item_count, total: totalAmount }, () => {
        if (this.state.total > 0) {
          this.props.showCheckout(true);
        } else {
          this.props.showCheckout(false);
        }
      });
    }
  }

  _i_addClick() {
    const { i_count, total } = this.state;
    let item_count = i_count;
    let totalAmount = total;

    item_count += 1;
    totalAmount += parseInt(this.props.item.price_iron);
    this.setState({ i_count: item_count, total: totalAmount }, () => {
      if (this.state.total > 0) {
        this.props.showCheckout(true);
      } else {
        this.props.showCheckout(false);
      }
    });
  }

  _i_subClick() {
    const { i_count, total } = this.state;
    let item_count = i_count;
    let totalAmount = total;
    if (item_count > 0) {
      item_count -= 1;
      totalAmount -= parseInt(this.props.item.price_iron);
      this.setState({ i_count: item_count, total: totalAmount }, () => {
        if (this.state.total > 0) {
          this.props.showCheckout(true);
        } else {
          this.props.showCheckout(false);
        }
      });
    }
  }
  render() {
    const { item } = this.props;
    return (
      <View style={styles.priceItemWraper}>
        <View style={styles.priceItemImageWraper}>
          <Image
            source={item.image}
            resizeMode="contain"
            style={styles.priceItemImage}
          />
        </View>
        <View style={styles.priceItemTextWraper}>
          <Text type="base4">{item.name}</Text>

          <View style={styles.priceItemPriceWraper}>
            <View style={styles.priceItemRateWraper}>
              <Text
                type="base4"
                color={Colors.text.secondary}
                size="xSmall"
                style={AppStyles.flex}
              >
                Wash & Iron
              </Text>
              <View style={[AppStyles.flexRow, AppStyles.centerInner]}>
                <Text type="base4" size="xxxSmall" style={AppStyles.mRight10}>
                  {"AED " + item.price_was_iron}
                  <Text type="base4" size="xxxxSmall">
                    {"/Piece"}
                  </Text>
                </Text>
                <TouchableOpacity
                  activeOpacity={appActiveOpacity}
                  style={styles.addSub}
                  onPress={() => {
                    this._w_i_subClick();
                  }}
                >
                  <Image source={Images.sub} />
                </TouchableOpacity>
                <Text
                  type="base4"
                  color={Colors.text.secondary}
                  size="xSmall"
                  style={AppStyles.mRight10}
                >
                  {this.state.w_i_count}
                </Text>
                <TouchableOpacity
                  activeOpacity={appActiveOpacity}
                  style={styles.addSub}
                  onPress={() => this._w_i_addClick()}
                >
                  <Image source={Images.add} />
                </TouchableOpacity>
              </View>
            </View>

            <View style={[styles.priceItemRateWraper, { marginTop: 10 }]}>
              <Text
                type="base4"
                color={Colors.text.secondary}
                size="xSmall"
                style={AppStyles.flex}
              >
                {"Iron Only"}
              </Text>
              <View style={[AppStyles.flexRow, AppStyles.centerInner]}>
                <Text type="base4" size="xxxSmall" style={AppStyles.mRight10}>
                  {"AED " + item.price_iron}
                  <Text type="base4" size="xxxxSmall">
                    {"/Piece"}
                  </Text>
                </Text>
                <TouchableOpacity
                  activeOpacity={appActiveOpacity}
                  style={styles.addSub}
                  onPress={() => {
                    this._i_subClick();
                  }}
                >
                  <Image source={Images.sub} />
                </TouchableOpacity>
                <Text
                  type="base4"
                  color={Colors.text.secondary}
                  size="xSmall"
                  style={AppStyles.mRight10}
                >
                  {this.state.i_count}
                </Text>
                <TouchableOpacity
                  activeOpacity={appActiveOpacity}
                  style={styles.addSub}
                  onPress={() => {
                    this._i_addClick();
                  }}
                >
                  <Image source={Images.add} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.lineView} />
          <View style={styles.totalWraper}>
            <Text type="base4" color={Colors.text.secondary} size="xxxSmall">
              Total:
              <Text type="base4" size="xxxSmall">
                {" "}
                AED
              </Text>
              <Text type="base4" size="xSmall">
                {" " + this.state.total}
              </Text>
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
