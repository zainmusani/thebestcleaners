// @flow
import { StyleSheet } from "react-native";
import { Colors } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent
  },
  priceItemWraper: {
    margin: 15,
    backgroundColor: "white",
    borderRadius: 10,
    flexDirection: "row",
    marginBottom: 5,
    marginTop: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1
  },
  priceItemImageWraper: {
    margin: 5,
    overflow: "hidden",
    borderRadius: 10,
    borderColor: Colors.itemBorder,
    borderWidth: 0.3,
    height: 124,
    width: 82,
    alignItems: "center",
    justifyContent: "center"
  },
  priceItemImage: { height: 80, width: 80 },
  priceItemTextWraper: {
    padding: 10,
    paddingRight: 10,
    paddingLeft: 5,
    flex: 1
  },
  priceItemPriceWraper: {
    justifyContent: "flex-start",
    marginTop: 5
  },
  priceItemPrice: {
    justifyContent: "space-around",
    flex: 1
  },
  priceItemRateWraper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  addSub: {
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    marginRight: 10,
    borderColor: Colors.background.primary,
    height: 21.5,
    width: 21.5,
    borderRadius: 21.5 / 2
  },
  lineView: {
    height: 1,
    width: "100%",
    marginTop: 12,
    backgroundColor: Colors.itemBorder
  },
  totalWraper: {
    alignItems: "flex-end",
    paddingRight: 5,
    marginTop: 10
  }
});
