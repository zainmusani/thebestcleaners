import React, { Component } from "react";
import { connect } from "react-redux";
import { View, TouchableOpacity, Image, FlatList } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { Images, Colors, AppStyles } from "../../theme";
import {
  appTexts,
  PRICE_ORDER_CATEGORIES,
  appActiveOpacity
} from "../../constants";
import styles from "./styles";
import PropTypes from "prop-types";
import { Text, OrderItem } from "../";
import Util from "../../util";

class PriceNewOrderList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      catogories: props.priceCatogiries.catogories[0],
      selected: 0
    };
  }
  static propTypes = {
    isPriceList: PropTypes.bool,
    showCheckout: PropTypes.func
  };

  static defaultProps = {
    isPriceList: false
  };
  flatList;

  _renderTopButtons() {
    const { selected } = this.state;

    return (
      <View style={{ flexDirection: "row" }}>
        {PRICE_ORDER_CATEGORIES.map((item, index) => {
          const isSelected = selected === index;
          return (
            <TouchableOpacity
              activeOpacity={appActiveOpacity}
              style={isSelected ? styles.tabButtomSelected : styles.tabButtom}
              onPress={() => this.onCategorySwitch(index)}
            >
              <Image source={isSelected ? item.selectedIcon : item.icon} />
              <Text
                type="base4"
                style={AppStyles.mTop10}
                color={isSelected ? Colors.text.primary : Colors.text.tertiary}
                size="xxxSmall"
              >
                {item.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
  onCategorySwitch = selected => {
    const catogories = this.props.priceCatogiries.catogories[selected];
    this.flatList.scrollToOffset({ animated: true, offset: 0 });
    this.setState({ catogories, selected });
  };

  _renderCatogries() {
    const items = this.state.catogories.data;

    return this.props.isPriceList ? (
      <FlatList
        showsVerticalScrollIndicator={false}
        ref={ref => (this.flatList = ref)}
        data={items}
        renderItem={this.renderPriceItem}
      />
    ) : (
      <FlatList
        showsVerticalScrollIndicator={false}
        ref={ref => (this.flatList = ref)}
        data={items}
        renderItem={({ item }) => (
          <OrderItem
            showCheckout={show => this.props.showCheckout(show)}
            item={item}
          />
        )}
      />
    );
  }
  renderPriceItem = ({ item }) => {
    return (
      <View style={styles.priceItemWraper}>
        <View style={styles.priceItemImageWraper}>
          <Image
            source={item.image}
            resizeMode="contain"
            style={styles.priceItemImage}
          />
        </View>
        <View style={styles.priceItemTextWraper}>
          <Text type="base4">{item.name}</Text>

          <View style={styles.priceItemPriceWraper}>
            <View style={styles.priceItemRateWraper}>
              <Text type="base4" color={Colors.text.secondary} size="xSmall">
                {"Wash & Iron"}
              </Text>
              <Text type="base4" size="xSmall">
                {"AED " + item.price_was_iron}
                <Text type="base4" size="xxSmall">
                  {"/Piece"}
                </Text>
              </Text>
            </View>

            <View style={styles.priceItemRateWraper}>
              <Text type="base4" color={Colors.text.secondary} size="xSmall">
                {"Iron Only"}
              </Text>
              <Text type="base4" size="xSmall">
                {"AED " + item.price_iron}
                <Text type="base4" size="xxSmall">
                  {"/Piece"}
                </Text>
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderTopButtons()}
        {this._renderCatogries()}
      </LinearGradient>
    );
  }
}
const mapStateToProps = ({ general }) => ({
  priceCatogiries: general.priceCatogiries
});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(PriceNewOrderList);
