// @flow
import { StyleSheet } from "react-native";
import { Colors, Metrics, AppStyles } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1
    // backgroundColor: Colors.grey6
    // ...AppStyles.centerInner
  },
  image: {
    width: "100%",
    height: "100%",
    ...AppStyles.flex,
    ...AppStyles.spaceBetween
  },
  topImage: { width: "100%", height: 100 },
  tabButtom: {
    backgroundColor: Colors.background.secondary,
    height: 62,
    width: "33.3%",
    justifyContent: "center",
    alignItems: "center",
    borderRightColor: Colors.buttonBorder.dark,
    borderRightWidth: 1,
    borderLeftColor: Colors.buttonBorder.lite,
    borderLeftWidth: 1
  },
  tabButtomSelected: {
    backgroundColor: Colors.transparent,
    height: 62,
    width: "33.3%",
    justifyContent: "center",
    alignItems: "center",
    borderRightColor: Colors.buttonBorder.dark,
    borderRightWidth: 1,
    borderLeftColor: Colors.buttonBorder.lite,
    borderLeftWidth: 1
  },
  priceItemWraper: {
    margin: 15,
    backgroundColor: "white",
    borderRadius: 10,
    flexDirection: "row",
    marginBottom: 5,
    marginTop: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1
  },
  priceItemImageWraper: {
    margin: 5,
    overflow: "hidden",
    borderRadius: 10,
    borderColor: Colors.itemBorder,
    borderWidth: 0.3,
    height: 104,
    width: 82,
    alignItems: "center",
    justifyContent: "center"
  },
  priceItemImage: { height: 80, width: 80 },
  priceItemTextWraper: {
    padding: 15,
    paddingRight: 10,
    flex: 1
  },
  priceItemPriceWraper: {
    justifyContent: "space-around",
    flex: 1,
    paddingTop: 10
  },
  priceItemPrice: {
    justifyContent: "space-around",
    flex: 1,
    paddingTop: 10
  },
  priceItemRateWraper: {
    flexDirection: "row",
    justifyContent: "space-between"
  }
});
