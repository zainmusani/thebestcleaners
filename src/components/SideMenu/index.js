// @flow
import React from "react";
import { View, Image, Linking, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, ButtonView } from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import { lorenIpsam } from "../../constants";

const DRAWER_ITEMS = [
  {
    text: "Order History",
    onPress: () => Actions.jump("notification_tab"),
    Image: Images.order_history_drawer
  },
  {
    text: "Price Details",
    onPress: () => Actions.jump("price_tab"),
    Image: Images.price
  },
  {
    text: "New Orders",
    onPress: () => Actions.jump("new_order_tab"),
    Image: Images.order
  },
  {
    text: "Our Services",
    onPress: () => Actions.jump("our_services_tab"),
    Image: Images.services
  },
  {
    text: "About Us",
    onPress: () => {
      Actions.content({ title: "About Us ", detail: lorenIpsam });
    },
    Image: Images.about_us_drawer
  },
  {
    text: "Privacy Policy",
    onPress: () => {
      Actions.content({ title: "Privacy Policy", detail: lorenIpsam });
    },
    Image: Images.privacy_policy_drawer
  },
  {
    text: "Terms & Condition",
    onPress: () => {
      Actions.content({ title: "Terms & Condition", detail: lorenIpsam });
    },
    Image: Images.terms_drawer
  },
  {
    text: "Contact Us",
    Image: Images.contact_drawer,
    onPress: () => {
      Actions.contact();
    }
  },
  {
    text: "Logout",
    onPress: () => Actions.reset("login"),
    Image: Images.logout_drawer
  }
];

export default class SideMenu extends React.PureComponent {
  static propTypes = {};

  static defaultProps = {};

  renderUserDetails() {
    return (
      <View style={styles.userDetailsWrapper}>
        <Text color={Colors.text.tertiary}>Musnah Fahada Antoun</Text>
        <Text size="small" style={AppStyles.mTop10} textAlign="left">
          musnah@gmail.com
        </Text>
      </View>
    );
  }
  on_press(element) {
    element.onPress();
    Actions.drawerClose();
  }
  renderOptionsList() {
    return (
      <ScrollView style={[AppStyles.flex, AppStyles.padding10]}>
        {DRAWER_ITEMS.map((element, index) => (
          <ButtonView
            style={styles.listItem}
            key={index}
            onPress={() => {
              // Actions.drawerClose();
              setTimeout(() => {
                element.onPress();
              }, 500);
              // Actions.drawerClose();
            }}
          >
            <Image source={element.Image} />
            <Text
              style={{ letterSpacing: 1, marginStart: 20 }}
              color={Colors.text.tertiary}
            >
              {element.text}
            </Text>
          </ButtonView>
        ))}
      </ScrollView>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderUserDetails()}
        {this.renderOptionsList()}
      </View>
    );
  }
}
