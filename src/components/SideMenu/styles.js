// @flow
import { StyleSheet } from "react-native";
import { Colors, AppStyles, Metrics } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background.secondary
  },
  userDetailsWrapper: {
    backgroundColor: Colors.background.secondary,
    paddingTop: Metrics.statusBarHeight + 20,
    paddingBottom: 30,
    paddingHorizontal: 20,
    borderBottomWidth: 0.4,
    borderColor: Colors.background.primary
  },
  imageWrapper: {
    width: 100,
    height: 100,
    borderRadius: 100,
    overflow: "hidden",
    ...AppStyles.mBottom20
  },
  userImage: { width: 100, height: 100 },
  listItem: {
    padding: 10,
    ...AppStyles.pTop15,
    ...AppStyles.pBottom15,
    ...AppStyles.flexRow
  }
});
