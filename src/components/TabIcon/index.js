// @flow
import _ from "lodash";
import React from "react";
import PropTypes from "prop-types";
import { Image, View } from "react-native";
import Text from "../Text";
import { Images, Metrics, AppStyles } from "../../theme";
import Util from "../../util";

const TabIcon = ({ title, focused }) => {
  return (
    <View
      style={[
        {
          backgroundColor: "red",
          borderColor: "green",
          borderWidth: 1,
          flex: 1
        }
      ]}
    >
      {/* <Image
        resizeMode="contain"
        style={[
          {
            width: Metrics.icon.small,
            height: Metrics.icon.small
          }
        ]}
        source={Images[title.toLocaleLowerCase()]}
      /> */}
    </View>
  );
};

TabIcon.propTypes = {
  title: PropTypes.string.isRequired,
  focused: PropTypes.bool.isRequired
};

export default TabIcon;
