// @flow
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { View, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, ButtonView } from "../";
import { setSelectedTab } from "../../actions/GenerealActions";
import styles from "./styles";
import LinearGradient from "react-native-linear-gradient";
import { Images, Colors, AppStyles } from "../../theme";

const BUTTON_TYPES = {
  icon: "icon",
  textIcon: "textIcon"
};

const tabsData = [
  {
    name: "home",
    image: Images.home,
    onPress: () => Actions.jump("home_tab")
  },
  {
    name: "price",
    image: Images.price,
    onPress: () => Actions.jump("price_tab")
  },
  {
    name: "order",
    image: Images.order,
    onPress: () => Actions.jump("new_order_tab")
  },
  {
    name: "services",
    image: Images.services,
    onPress: () => Actions.jump("our_services_tab")
  },
  {
    name: "notifications",
    image: Images.notification,
    onPress: () => Actions.jump("notification_tab")
  }
];

class Tabbar extends React.PureComponent {
  static propTypes = {
    selectedIndex: PropTypes.number.isRequired,
    setSelectedTab: PropTypes.func.isRequired,
    defaultTabbar: PropTypes.bool
  };

  static defaultProps = {
    defaultTabbar: true
  };

  renderSelectedBar() {
    return (
      <LinearGradient
        colors={Colors.tabbar.selectedGradient}
        start={AppStyles.gradientValues.mainButtonStart}
        end={AppStyles.gradientValues.mainButtonEnd}
        style={styles.selectedTabIndicator}
      />
    );
  }

  render() {
    const { selectedIndex, defaultTabbar } = this.props;
    // const selectedIndex = 4;
    const data = true ? tabsData : livematchtabsData;
    return (
      <View style={styles.container}>
        {data.map((element, index) => {
          return (
            <ButtonView
              key={index}
              style={styles.itemWrapper}
              onPress={() => {
                if (index !== 7676) {
                  this.props.setSelectedTab(index);
                }

                element.onPress();
              }}
            >
              <View style={styles.btn1}>
                <Image source={element.image} />
              </View>
              {selectedIndex === index && this.renderSelectedBar()}
            </ButtonView>
          );
        })}
      </View>
    );
  }
}

const mapStateToProps = ({ general }) => ({
  selectedIndex: general.selectedIndex
});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(Tabbar);
