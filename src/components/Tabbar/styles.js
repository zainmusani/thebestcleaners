// @flow
import { StyleSheet } from "react-native";
import { isIphoneX } from "react-native-iphone-x-helper";
import { Colors, Metrics, AppStyles } from "../../theme";

export default StyleSheet.create({
  container: {
    height: Metrics.tabBarHeight,
    // paddingBottom: isIphoneX() ? 83 : 49,
    /* height: 49,
    marginBottom: 34, */
    backgroundColor: Colors.tabbar.background,
    shadowColor: Colors.white,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    ...AppStyles.flexRow,
    ...AppStyles.spaceAround
    // ...AppStyles.alignItemsCenter
  },
  btn1: {
    width: 50,
    height: 35,
    ...AppStyles.centerInner
  },
  btn2: {
    backgroundColor: Colors.greenTint,
    padding: 7,
    paddingHorizontal: 10,
    justifyContent: "center",
    ...AppStyles.flexRow,
    borderRadius: 20,
    height: 34,
    ...AppStyles.centerInner
  },
  btn2Image: {
    marginRight: 5
  },
  selectedBar: {
    width: 20,
    height: 3,
    borderRadius: 2,
    backgroundColor: Colors.green,
    position: "absolute",
    top: 0
  },
  itemWrapper: {
    paddingTop: 10,
    alignItems: "center",
    flex: 1
  },
  selectedTabIndicator: {
    backgroundColor: Colors.danger,
    height: 5,
    left: 0,
    right: 0,
    position: "absolute"
  }
});
