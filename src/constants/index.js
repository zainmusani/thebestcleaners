import { Images } from "../theme";

// export const TIME_ZONE = (-1 * new Date().getTimezoneOffset()) / 60;
export const APP_URL = "";
export const APP_DOMAIN = "";
export const QUERY_LIMIT = 10;
export const SAGA_ALERT_TIMEOUT = 500;

// date time formats
export const DATE_FORMAT1 = "dddd, DD MMMM, YYYY";

// Messages

export const LOCATION_PERMISSION_DENIED_ERROR2 =
  "Location permission required, please go to app settings to allow access";
export const INVALID_NAME_ERROR = "Invalid name";
export const INVALID_EMAIL_ERROR = "Invalid email";
export const INTERNET_ERROR = "Please connect to the working internet";
export const SESSION_EXPIRED_ERROR = "Session expired, Please login again";

// Message types
export const MESSAGE_TYPES = {
  INFO: "info",
  ERROR: "error",
  SUCCESS: "success"
};

export const appTexts = {
  welcomeTagLine: "The Best Laundry! Professional Dry Cleaners",
  welcomeTagLineStart: "The Best Laundry!",
  welcomeTagLineEnd: "Professional Dry Cleaners",
  ForgotPass: "Forgot Password?",
  dontHaveAcc: "Don't have an account?"
};

export const appActiveOpacity = 0.7;
export const lorenIpsam =
  "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose ";
//Home ContentList
export const HOME_CONTENT = [
  {
    title: "PRICE DETAILS",
    details: "Lorem ipsum dolor sit amet, \nconsectetur adipiscing elitsed",
    image: Images.home_ledger,
    action: () => alert("PRICE DETAILS")
  },
  {
    title: "NEW ORDERS",
    details: "Lorem ipsum dolor sit amet, \nconsectetur adipiscing elitsed",
    image: Images.home_ledger,
    action: () => alert("NEW ORDERS")
  },
  {
    title: "OUR SERVICES",
    details: "Lorem ipsum dolor sit amet, \nconsectetur adipiscing elitsed",
    image: Images.home_ledger,
    action: () => alert("OUR SERVICES")
  }
];
export const PRICE_ORDER_CATEGORIES = [
  {
    title: "CLOTH",
    icon: Images.cloths_tab,
    selectedIcon: Images.cloths_tab_blue
  },
  {
    title: "SHOES & BAG",
    icon: Images.shoes_tab,
    selectedIcon: Images.shoes_tab_blue
  },
  {
    title: "UPHOLSTERY",
    icon: Images.sofa_tab,
    selectedIcon: Images.sofa_tab_blue
  }
];
// File Types
export const FILE_TYPES = { VIDEO: "video", IMAGE: "image", AUDIO: "audi" };
