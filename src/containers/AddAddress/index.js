// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Text,
  CustomNavbar,
  PriceNewOrderList,
  AppTextInput,
  GradientButton
} from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class AddAddress extends Component {
  last_name;
  first_name;
  _renderNavbar() {
    return (
      <CustomNavbar
        title="ADD ADDRESS"
        titleColor="white"
        hasBorder={false}
        hasBack={true}
      />
    );
  }
  _onSubmitTitle = () => {
    this.first_name.focus();
  };
  _onSubmitFirst = () => {
    this.last_name.focus();
  };
  renderAddressForm() {
    return (
      <View style={{ paddingBottom: 40 }}>
        <AppTextInput
          label="Title"
          ref={ref => {
            this.email = ref;
          }}
          icon={Images.home_blue}
          onSubmitEditing={() => this._onSubmitTitle()}
        />
        <AppTextInput
          label="First Name"
          ref={ref => {
            this.first_name = ref;
          }}
          icon={Images.user_name}
          onSubmitEditing={() => this._onSubmitFirst()}
        />
        <AppTextInput
          label="Last Name"
          ref={ref => (this.last_name = ref)}
          icon={Images.user_name}
          onChangeText={console.log("asd")}
        />
        <AppTextInput
          label="Location"
          icon={Images.location}
          secureTextEntry={true}
        />

        <AppTextInput
          label="Address"
          ref={ref => (this.address = ref)}
          icon={Images.address}
          secureTextEntry={false}
        />

        <GradientButton
          style={AppStyles.mTop30}
          color="white"
          onPress={() => Actions.pop()}
        >
          SAVE ADDRESS
        </GradientButton>
      </View>
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
        >
          <View style={styles.formContainer}>{this.renderAddressForm()}</View>
        </KeyboardAwareScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(AddAddress);
