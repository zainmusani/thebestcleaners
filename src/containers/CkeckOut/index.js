// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput
} from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Text,
  CustomNavbar,
  PriceNewOrderList,
  GradientButton
} from "../../components";
import _ from "lodash";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";
import CheckBox from "react-native-check-box";
import Modal from "react-native-modal";
import { isTerminatorless } from "@babel/types";
import ActionSheet from "react-native-actionsheet";

class CkeckOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: false,
      timeModal: false,
      deliveryModal: false,
      paymentModal: false,
      noteModal: false,
      pick_time: "Select Time Slot",
      deliver_time: "Select Time Slot",
      deleteOrder: {
        index: 0,
        item: "empty"
      },
      order: [
        {
          item: "Shirt",
          wash: 2,
          wash_price: 15,
          iron: 3,
          iron_price: 10
        },
        {
          item: "Pent",
          wash: 3,
          wash_price: 20,
          iron: 4,
          iron_price: 12
        },
        {
          item: "Jacket",
          wash: 2,
          wash_price: 50,
          iron: 1,
          iron_price: 30
        }
      ]
    };
  }

  _renderNavbar() {
    return (
      <CustomNavbar
        title="CHECK OUT"
        titleColor="white"
        hasBorder={true}
        hasBack={true}
      />
    );
  }

  _renderTime() {
    return (
      <TouchableOpacity
        activeOpacity={appActiveOpacity}
        style={styles.itemWraper}
        // onPress={() => {
        //   this.setState({ timeModal: true });
        // }}

        onPress={() => {
          Actions.dateTimePicker({
            title: "Pick Time",
            onTimeUpdate: dateTime => {
              this.setState({ pick_time: dateTime });
            }
          });
        }}
      >
        <View style={AppStyles.flexRow}>
          <View style={AppStyles.flex}>
            <Text type="base4">Pick a Time</Text>
            <View
              style={[
                AppStyles.flexRow,
                AppStyles.alignItemsCenter,
                AppStyles.pLeft5,
                AppStyles.mTop10
              ]}
            >
              <Image
                resizeMode="contain"
                source={Images.clock_icon}
                style={{ height: 20, width: 20 }}
              />
              <Text
                style={AppStyles.mLeft5}
                size="xSmall"
                color={Colors.text.secondary}
              >
                {typeof this.state.pick_time === "string"
                  ? this.state.pick_time
                  : this.state.pick_time.format("LLLL")}
              </Text>
            </View>
          </View>
          <View style={AppStyles.centerInner}>
            <Image source={Images.arrow_right} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderTimeModal() {
    return (
      <View>
        <Modal isVisible={this.state.timeModal}>
          <View style={styles.modalContainer}>
            <TouchableOpacity
              activeOpacity={appActiveOpacity}
              onPress={() => {
                this.setState({ timeModal: false, deliveryModal: false });
              }}
              style={styles.closewraper}
            >
              <Image
                source={Images.close_icon}
                resizeMode="contain"
                style={{ height: 12, width: 12 }}
              />
            </TouchableOpacity>
            <Text>{this.state.deliveryModal ? "Delivery" : "Pick a Time"}</Text>
            <View style={styles.lineView} />
            <View style={styles.fieldsWraper}>
              <TouchableOpacity
                activeOpacity={appActiveOpacity}
                style={styles.fieldWraper}
              >
                <Text color={Colors.text.secondary} style={AppStyles.flex}>
                  Time
                </Text>
                <Image source={Images.arrow_down} />
              </TouchableOpacity>
              <View
                style={[styles.lineView, AppStyles.mTop15, AppStyles.mBottom15]}
              />
              <TouchableOpacity
                activeOpacity={appActiveOpacity}
                style={styles.fieldWraper}
              >
                <Text color={Colors.text.secondary} style={AppStyles.flex}>
                  Date
                </Text>
                <Image source={Images.arrow_down} />
              </TouchableOpacity>
            </View>
            <GradientButton
              color="white"
              onPress={() =>
                this.setState({ timeModal: false, deliveryModal: false })
              }
            >
              Done
            </GradientButton>
          </View>
        </Modal>
      </View>
    );
  }

  _renderPaymentModal() {
    return (
      <View>
        <Modal isVisible={this.state.paymentModal}>
          <View style={styles.PaymentModalContainer}>
            <View style={[AppStyles.padding15, { paddingTop: 20 }]}>
              <TouchableOpacity
                activeOpacity={appActiveOpacity}
                onPress={() => {
                  this.setState({ paymentModal: false });
                }}
                style={styles.closewraper}
              >
                <Image
                  source={Images.close_icon}
                  resizeMode="contain"
                  style={{ height: 12, width: 12 }}
                />
              </TouchableOpacity>
              <Text type="base3">{"Payment Method"}</Text>
              <View style={styles.lineView} />
              <View style={styles.paymentFieldsWraper}>
                <View
                  activeOpacity={appActiveOpacity}
                  style={styles.PaymentFieldWraper}
                >
                  <Image source={Images.cash_icon} />
                  <Text
                    color={Colors.text.secondary}
                    size="xxxSmall"
                    textAlign="center"
                    style={[
                      AppStyles.flex,
                      AppStyles.pLeft10,
                      AppStyles.pRight10
                    ]}
                  >
                    {"Lorem ipsum dolor amet, consectetur adipiscing."}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ paymentModal: false });
                    }}
                    activeOpacity={appActiveOpacity}
                    style={styles.paymentMethodButton}
                  >
                    <Text
                      size="xSmall"
                      type="base3"
                      color={Colors.text.tertiary}
                    >
                      CASH
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    styles.lineView,
                    AppStyles.mTop15,
                    AppStyles.mBottom15
                  ]}
                />
                <View
                  activeOpacity={appActiveOpacity}
                  style={styles.PaymentFieldWraper}
                >
                  <Image source={Images.card_selection_icon} />
                  <Text
                    color={Colors.text.secondary}
                    size="xxxSmall"
                    textAlign="center"
                    style={[
                      AppStyles.flex,
                      AppStyles.pLeft10,
                      AppStyles.pRight10
                    ]}
                  >
                    {"Lorem ipsum dolor amet, consectetur adipiscing."}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={appActiveOpacity}
                    onPress={() => {
                      this.setState({ paymentModal: false });
                    }}
                    style={[
                      styles.paymentMethodButton,
                      { backgroundColor: Colors.paymentBtnBlue }
                    ]}
                  >
                    <Text
                      size="xSmall"
                      type="base3"
                      color={Colors.text.tertiary}
                    >
                      CARD
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View
              style={{
                backgroundColor: "#EBF0FB",
                padding: 20,
                borderRadius: 15
              }}
            >
              <Text
                color={Colors.text.secondary}
                size="xxxSmall"
                textAlign="center"
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit do
                eiusmod tempor incididunt ut labore et dolore Ut enim ad minim.
              </Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
  _renderDelivery() {
    return (
      <TouchableOpacity
        activeOpacity={appActiveOpacity}
        style={styles.itemWraper}
        // onPress={() => this.setState({ timeModal: true, deliveryModal: true })}
        onPress={() => {
          Actions.dateTimePicker({
            title: "Delivery Time",
            onTimeUpdate: dateTime => {
              this.setState({ deliver_time: dateTime });
            }
          });
        }}
      >
        <View style={AppStyles.flexRow}>
          <View style={AppStyles.flex}>
            <Text type="base4">Delivery</Text>
            <View
              style={[
                AppStyles.flexRow,
                AppStyles.alignItemsCenter,
                AppStyles.pLeft5,
                AppStyles.mTop10
              ]}
            >
              <Image
                resizeMode="contain"
                source={Images.clock_icon}
                style={{ height: 20, width: 20 }}
              />
              <Text
                style={AppStyles.mLeft5}
                size="xSmall"
                color={Colors.text.secondary}
              >
                {typeof this.state.deliver_time === "string"
                  ? this.state.deliver_time
                  : this.state.deliver_time.format("LLLL")}
              </Text>
            </View>
          </View>
          <View style={AppStyles.centerInner}>
            <Image source={Images.arrow_right} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderDeliveryAddress() {
    return (
      <TouchableOpacity
        activeOpacity={appActiveOpacity}
        onPress={() => Actions.delivery()}
        style={styles.itemWraper}
      >
        <View style={AppStyles.flexRow}>
          <View style={AppStyles.flex}>
            <Text type="base4">Delivery Address</Text>
            <View
              style={[
                AppStyles.flexRow,
                AppStyles.alignItemsCenter,
                AppStyles.pLeft5,
                AppStyles.mTop10
              ]}
            >
              <Image
                resizeMode="contain"
                source={Images.checkOut_location_icon}
                style={{ height: 20, width: 20 }}
              />
              <Text
                style={AppStyles.mLeft5}
                size="xSmall"
                color={Colors.text.secondary}
              >
                231 Fujairah, United Arab Emirates
              </Text>
            </View>
            <View style={styles.lineView} />
            <View
              style={[
                AppStyles.flexRow,
                AppStyles.alignItemsCenter,
                AppStyles.pLeft5,
                AppStyles.mTop20
              ]}
            >
              <Text
                style={[AppStyles.mLeft5, AppStyles.flex]}
                size="xSmall"
                color={Colors.text.secondary}
              >
                House No 13225
              </Text>
              <Image
                resizeMode="contain"
                source={Images.check_icon}
                style={{ height: 20, width: 20 }}
              />
            </View>
          </View>
          <View style={AppStyles.mTop20}>
            <Image source={Images.arrow_right} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderPaymentInfo() {
    return (
      <TouchableOpacity
        activeOpacity={appActiveOpacity}
        onPress={() => this.setState({ paymentModal: true })}
        style={styles.itemWraper}
      >
        <View style={AppStyles.flexRow}>
          <View style={AppStyles.flex}>
            <Text type="base4">Payment Information</Text>
            <View
              style={[
                AppStyles.flexRow,
                AppStyles.alignItemsCenter,
                AppStyles.pLeft5,
                AppStyles.mTop10
              ]}
            >
              <Image
                source={Images.credit_card_icon}
                resizeMode="contain"
                style={{ height: 20, width: 20 }}
              />
              <Text
                style={AppStyles.mLeft5}
                size="xSmall"
                color={Colors.text.secondary}
              >
                Cash
              </Text>
            </View>
          </View>
          <View style={AppStyles.centerInner}>
            <Image source={Images.arrow_right} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderSpecialNote() {
    return (
      <TouchableOpacity
        activeOpacity={appActiveOpacity}
        onPress={() => this.setState({ noteModal: true })}
        style={styles.itemWraper}
      >
        <View style={AppStyles.flexRow}>
          <View style={AppStyles.flex}>
            <Text type="base4">Special Note</Text>
            <View
              style={[
                AppStyles.flexRow,
                AppStyles.alignItemsCenter,
                AppStyles.pLeft5,
                AppStyles.mTop10
              ]}
            >
              <Image
                source={Images.notepad_icon}
                resizeMode="contain"
                style={{ height: 20, width: 20 }}
              />
              <Text
                style={AppStyles.mLeft5}
                size="xSmall"
                color={Colors.text.secondary}
              >
                Any instruction for us to follow
              </Text>
            </View>
          </View>
          <View style={AppStyles.centerInner}>
            <Image source={Images.arrow_right} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  _renderNoteModal() {
    return (
      <View>
        <Modal isVisible={this.state.noteModal} avoidKeyboard={true}>
          <View style={styles.modalContainer}>
            <TouchableOpacity
              activeOpacity={appActiveOpacity}
              onPress={() => {
                this.setState({ noteModal: false });
              }}
              style={styles.closewraper}
            >
              <Image
                source={Images.close_icon}
                resizeMode="contain"
                style={{ height: 12, width: 12 }}
              />
            </TouchableOpacity>
            <Text>{"Special Note"}</Text>
            <View style={styles.lineView} />
            <TextInput
              placeholder="Any Special note regarding order"
              multiline={true}
              textAlign="left"
              style={styles.noteFieldsWraper}
            />
            <GradientButton
              color="white"
              onPress={() => this.setState({ noteModal: false })}
            >
              Done
            </GradientButton>
          </View>
        </Modal>
      </View>
    );
  }
  _renderExpress() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: 15
        }}
      >
        <CheckBox
          onClick={() => {
            this.setState({
              isChecked: !this.state.isChecked
            });
          }}
          isChecked={this.state.isChecked}
          rightTextView={
            <Text style={AppStyles.mLeft5} size="xSmall">
              Express Order
            </Text>
          }
          checkedImage={<Image source={Images.checked_icon} />}
          unCheckedImage={<Image source={Images.unckeck_icon} />}
        />

        <Text size="xSmall">
          {"Delivery in 6 hours 30AED will \nbe charge additionally"}
        </Text>
      </View>
    );
  }
  _renderTex() {
    return (
      <TouchableOpacity
        activeOpacity={appActiveOpacity}
        style={styles.itemWraper}
      >
        <Text type="base4" color={Colors.text.secondary}>
          Tax Receipt
        </Text>
        <View
          style={[
            AppStyles.flexRow,
            AppStyles.alignItemsCenter,
            AppStyles.mTop20
          ]}
        >
          <Text style={AppStyles.flex}>Total</Text>
          <Text color={Colors.text.secondary}>AED 200.00</Text>
        </View>

        <View
          style={[
            AppStyles.flexRow,
            AppStyles.alignItemsCenter,
            AppStyles.mTop15
          ]}
        >
          <Text style={AppStyles.flex}>Tax</Text>
          <Text color={Colors.text.secondary}>AED 12.00</Text>
        </View>

        <View
          style={[
            AppStyles.flexRow,
            AppStyles.alignItemsCenter,
            AppStyles.mTop15
          ]}
        >
          <Text style={AppStyles.flex}>Service Charges</Text>
          <Text color={Colors.text.secondary}>AED 8.00</Text>
        </View>
        <View style={[styles.lineView, { marginTop: 20, marginBottom: 20 }]} />
        <View style={[AppStyles.flexRow, AppStyles.alignItemsCenter]}>
          <Text style={AppStyles.flex}>Sub Total</Text>
          <Text>AED 220.00</Text>
        </View>
      </TouchableOpacity>
    );
  }
  _renderPlaceOrder() {
    return (
      <GradientButton color="white" onPress={() => Actions.thank_you()}>
        PLACE ORDER
      </GradientButton>
    );
  }

  orderItemPress(index, item) {
    deleteOrder = {
      index: index,
      item: item
    };
    this.setState({ deleteOrder }, () => {
      this.ActionSheet.show();
    });
  }

  renderOrderDetails() {
    const { order } = this.state;
    return (
      <View style={styles.itemWraper}>
        <View
          style={[
            AppStyles.flex,
            AppStyles.flexRow,
            AppStyles.alignItemsCenter,
            { justifyContent: "space-between" }
          ]}
        >
          <Text type="base4">{"Order Details"}</Text>
          <TouchableOpacity
            activeOpacity={appActiveOpacity}
            onPress={() => Actions.pop()}
          >
            <Text color={Colors.text.secondary} size="xxSmall">
              Edit
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          {order.map((item, index) => {
            return (
              <View style={AppStyles.mTop5}>
                {item.wash > 0 && (
                  <TouchableOpacity
                    onPress={() => this.orderItemPress(index, "wash")}
                    activeOpacity={appActiveOpacity}
                    style={[
                      AppStyles.flexRow,
                      { justifyContent: "space-between" }
                    ]}
                  >
                    <Text size="small" color={Colors.text.secondary}>
                      {`${item.item} - Wash & Iron - ${item.wash}`}
                    </Text>
                    <Text size="xSmall" color={Colors.text.secondary}>
                      {"AED " + item.wash_price * item.wash}
                    </Text>
                  </TouchableOpacity>
                )}
                {item.iron > 0 && (
                  <TouchableOpacity
                    onPress={() => this.orderItemPress(index, "iron")}
                    activeOpacity={appActiveOpacity}
                    style={[
                      AppStyles.flexRow,
                      { justifyContent: "space-between" }
                    ]}
                  >
                    <Text size="small" color={Colors.text.secondary}>
                      {`${item.item} - Iron Only - ${item.iron}`}
                    </Text>
                    <Text size="xSmall" color={Colors.text.secondary} />
                    <Text size="xSmall" color={Colors.text.secondary}>
                      {"AED " + item.iron_price * item.iron}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            );
          })}
        </View>
      </View>
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderTimeModal()}
        {this._renderPaymentModal()}
        {this._renderNavbar()}
        {this._renderNoteModal()}
        <ScrollView contentContainerStyle={{ padding: 15 }}>
          {this._renderTime()}
          {this._renderDelivery()}
          {this._renderDeliveryAddress()}
          {this._renderPaymentInfo()}
          {this._renderSpecialNote()}
          {this.renderOrderDetails()}
          {this._renderExpress()}
          {this._renderTex()}
          {this._renderPlaceOrder()}
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            title={"Select Option"}
            options={["Delete", "Cancel"]}
            cancelButtonIndex={1}
            destructiveButtonIndex={0}
            onPress={index => {
              if (index == 0) {
                const { deleteOrder, order } = this.state;
                temp_order = _.cloneDeep(order);
                temp_order[deleteOrder.index][deleteOrder.item] = 0;
                this.setState({ order: temp_order });
              }
            }}
          />
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(CkeckOut);
