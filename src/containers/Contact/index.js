// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Text,
  CustomNavbar,
  PriceNewOrderList,
  AppTextInput,
  GradientButton,
  TextInput
} from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { setSelectedTab } from "../../actions/GenerealActions";
import _ from "lodash";
import {
  appTexts,
  INVALID_EMAIL_ERROR,
  INVALID_NAME_ERROR,
  appActiveOpacity
} from "../../constants";
import { hidden } from "ansi-colors";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      message: ""
    };
  }
  email;
  name;
  message;
  _renderNavbar() {
    return (
      <CustomNavbar
        title="CONTACT US"
        titleColor="white"
        hasBorder={false}
        hasBack={true}
      />
    );
  }
  _validate() {
    const { email, name, message } = this.state;
    if (_.isEmpty(email)) {
      // email is required
      Util.isRequiredMessage("email");
      this.email.focus();
      return false;
    }
    if (!Util.isEmailValid(email)) {
      // invalid email
      Util.topAlertError(INVALID_EMAIL_ERROR);
      this.email.focus();

      return false;
    }
    if (_.isEmpty(name)) {
      Util.isRequiredMessage("name");
      this.name.focus();
      return false;
    }
    if (!Util.isValidName(name)) {
      // invalid name
      Util.topAlertError(INVALID_NAME_ERROR);
      this.name.focus();

      return false;
    }
    if (_.isEmpty(message)) {
      Util.isRequiredMessage("message");
      this.message.focus();
      return false;
    }
    return true;
  }
  _formSubmit() {
    if (this._validate()) {
      Util.topAlert("feedback submitted successfully");
      Actions.pop();
    }
  }
  _onSubmitName = () => {
    this.message.focus();
  };
  _onSubmitEmail = () => {
    this.name.focus();
  };
  _renderForm() {
    const { email, name, message } = this.state;
    return (
      <View style={styles.formWraper}>
        <Image
          style={{ alignSelf: "center" }}
          source={Images.contacts_us}
          resizeMode="contain"
          style={{ height: 150, width: 150 }}
        />
        <View style={{ width: "100%", marginTop: 20 }}>
          <Text size="xLarge" type="bold" textAlign="center">
            Drop us a line
          </Text>
          <Text
            style={AppStyles.mTop10}
            color={Colors.text.secondary}
            textAlign="center"
          >
            We're here to help and answer any question you might have.
          </Text>
          <AppTextInput
            label="Email"
            ref={ref => (this.email = ref)}
            icon={Images.email}
            value={email}
            onChangeText={email => this.setState({ email })}
            onSubmitEditing={() => this._onSubmitEmail()}
          />
          <AppTextInput
            label="Name"
            ref={ref => (this.name = ref)}
            icon={Images.user_name}
            value={name}
            onChangeText={name => this.setState({ name })}
            onSubmitEditing={() => this._onSubmitName()}
          />
          <TextInput
            ref={ref => (this.message = ref)}
            value={message}
            multiline={true}
            placeholder="Message"
            onChangeText={message => this.setState({ message })}
          />
          <GradientButton
            style={AppStyles.mTop30}
            color="white"
            onPress={() => this._formSubmit()}
          >
            SUBMIT
          </GradientButton>
        </View>
      </View>
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <KeyboardAwareScrollView contentContainerStyle={{ paddingBottom: 30 }}>
          {this._renderForm()}
        </KeyboardAwareScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(Contact);
