// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, CustomNavbar, PriceNewOrderList } from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";

import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";

class Content extends Component {
  componentDidMount() {
    // Actions.drawerClose();
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        title={this.props.title.toUpperCase()}
        titleColor="white"
        hasBorder={false}
        hasBack={true}
      />
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <ScrollView>
          <View style={[AppStyles.padding10, AppStyles.centerInner]}>
            <Text style={AppStyles.mTop10} color={Colors.text.secondary}>
              {this.props.detail}
            </Text>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(Content);
