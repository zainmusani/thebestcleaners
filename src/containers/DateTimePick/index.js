// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, TouchableOpacity, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Text,
  CustomNavbar,
  PriceNewOrderList,
  GradientButton
} from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";
import DatePicker from "react-native-datepicker";
import moment from "moment";

class DateTimePick extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "",
      time: ""
    };
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        title={this.props.title.toUpperCase()}
        titleColor="white"
        hasBorder={false}
        hasBack={true}
      />
    );
  }
  ontimeDateSelect() {
    const { time, date } = this.state;
    if (time != "" && date != "") {
      let new_date = moment(date + " " + time);
      this.props.onTimeUpdate(new_date);
    }
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}

        <View style={styles.modalContainer}>
          <Text>{this.props.title}</Text>
          <View style={styles.lineView} />
          <View style={styles.fieldsWraper}>
            <View style={styles.fieldWraper}>
              <DatePicker
                style={AppStyles.flex}
                date={this.state.time}
                placeholder="Time"
                mode="time"
                minDate={new Date()}
                format="hh:mm a"
                maxDate="2050-06-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                customStyles={{
                  placeholderText: {
                    color: Colors.text.secondary,
                    fontSize: 18
                  },
                  dateText: { color: Colors.text.secondary, fontSize: 18 },
                  dateInput: {
                    borderWidth: 0,
                    alignItems: "flex-start",
                    justifyContent: "center"
                  }
                }}
                onDateChange={date => {
                  this.setState({ time: date }, () => {
                    this.ontimeDateSelect();
                  });
                }}
              />
              <Image source={Images.arrow_down} />
            </View>
            <View style={[styles.lineView]} />
            <View activeOpacity={appActiveOpacity} style={styles.fieldWraper}>
              <DatePicker
                style={{ flex: 1 }}
                date={this.state.date}
                mode="date"
                minDate={new Date()}
                placeholder="Date"
                format="MMM DD, YYYY"
                maxDate="2050-06-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                customStyles={{
                  placeholderText: {
                    color: Colors.text.secondary,
                    fontSize: 18
                  },
                  dateText: { color: Colors.text.secondary, fontSize: 18 },
                  dateInput: {
                    borderWidth: 0,
                    alignItems: "flex-start",
                    justifyContent: "center"
                  }
                }}
                onDateChange={date => {
                  this.setState({ date: date }, () => {
                    this.ontimeDateSelect();
                  });
                }}
              />
              <Image source={Images.arrow_down} />
            </View>
          </View>
          <GradientButton color="white" onPress={() => Actions.pop()}>
            Done
          </GradientButton>
        </View>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(DateTimePick);
