// @flow
import { StyleSheet } from "react-native";
import { Colors, Metrics } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  itemWraper: {
    marginBottom: 15,
    padding: 15,
    backgroundColor: "white",
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1
  },
  lineView: {
    height: 1,
    width: "100%",
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: Colors.itemBorder
  },
  modalContainer: {
    margin: 10,
    marginTop: 50,
    padding: 15,
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: "white",
    borderRadius: 15,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1
  },
  fieldWraper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  fieldsWraper: {
    backgroundColor: Colors.background.tertiary,
    padding: 20,
    paddingBottom: 10,

    paddingTop: 10,
    marginTop: 20,
    marginBottom: 30,
    borderRadius: 20,
    borderColor: Colors.modalItemBorder,
    borderWidth: 1,
    borderRightColor: Colors.modalItemBorder,
    borderRightWidth: 1
  },
  paymentFieldsWraper: {
    backgroundColor: Colors.background.tertiary,
    marginTop: 20,
    marginBottom: 30
  },
  paymentMethodButton: {
    backgroundColor: Colors.paymentBtnGreen,
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 20
  },
  PaymentFieldWraper: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10
  },
  PaymentModalContainer: {
    backgroundColor: "white",
    borderRadius: 15
  },
  noteFieldsWraper: {
    backgroundColor: Colors.background.tertiary,
    marginTop: 20,
    padding: 20,
    marginBottom: 20,
    borderRadius: 3,
    borderColor: Colors.modalItemBorder,
    borderWidth: 1,
    minHeight: 60
  }
});
