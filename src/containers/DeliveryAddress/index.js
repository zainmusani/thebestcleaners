// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, FlatList, TouchableOpacity, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, CustomNavbar, PriceNewOrderList } from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import _ from "lodash";
import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";

class DeliveryAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adds: [
        {
          title: "Delivery Address",
          address: "231 Fujairah, United Arab Emirates",
          detail: "House No 13225",
          selected: false
        },
        {
          title: "Office  Address",
          address: "231 Fujairah, United Arab Emirates",
          detail: "House No 13225",
          selected: true
        },
        {
          title: "Home  Address",
          address: "231 Fujairah, United Arab Emirates",
          detail: "House No 13225",
          selected: false
        }
      ]
    };
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        rightBtnImage={Images.washing_machine}
        title="DELIVERY ADDRESS"
        titleColor="white"
        hasBorder={false}
        hasBack={true}
        rightBtnImage={Images.add_nav_bar_icon}
        rightBtnPress={() => Actions.address()}
      />
    );
  }
  _itemOnclick(index) {
    adds = _.cloneDeep(this.state.adds);
    adds.forEach((element, ind) => {
      if (index === ind) {
        adds[ind].selected = true;
      } else {
        adds[ind].selected = false;
      }
    });
    this.setState({ adds });
  }
  render() {
    const { adds } = this.state;
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <FlatList
          data={adds}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity
                onPress={() => this._itemOnclick(index)}
                activeOpacity={appActiveOpacity}
                style={styles.itemWraper}
              >
                <View
                  style={[
                    AppStyles.flex,
                    AppStyles.flexRow,
                    AppStyles.alignItemsCenter,
                    { justifyContent: "space-between" }
                  ]}
                >
                  <Text size="small">{item.title}</Text>
                  <TouchableOpacity
                    activeOpacity={appActiveOpacity}
                    onPress={() => Actions.address()}
                  >
                    <Text color={Colors.text.secondary} size="xxSmall">
                      Edit
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    AppStyles.flexRow,
                    AppStyles.alignItemsCenter,
                    AppStyles.mTop10
                  ]}
                >
                  <Image source={Images.checkOut_location_icon} />
                  <Text
                    style={AppStyles.mLeft5}
                    color={Colors.text.secondary}
                    size="xxSmall"
                  >
                    {item.address}
                  </Text>
                </View>
                <View style={styles.lineView} />
                <View
                  style={[
                    AppStyles.flexRow,
                    AppStyles.flex,
                    { justifyContent: "space-between" }
                  ]}
                >
                  <Text color={Colors.text.secondary} size="xxSmall">
                    {item.detail}
                  </Text>
                  {item.selected && <Image source={Images.check_icon} />}
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={Util.keyExtractor}
        />
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(DeliveryAddress);
