// @flow
import { StyleSheet } from "react-native";
import { Colors, Metrics } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  itemWraper: {
    padding: 15,
    margin: 15,
    backgroundColor: "white",
    borderRadius: 10,
    marginBottom: 5,
    marginTop: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1
  },
  lineView: {
    height: 1,
    width: "100%",
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: Colors.itemBorder
  }
});
