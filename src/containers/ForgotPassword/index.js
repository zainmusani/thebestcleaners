// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Text,
  CustomNavbar,
  PriceNewOrderList,
  AppTextInput,
  GradientButton
} from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { setSelectedTab } from "../../actions/GenerealActions";
import _ from "lodash";
import {
  appTexts,
  INVALID_EMAIL_ERROR,
  appActiveOpacity,
  APP_DOMAIN
} from "../../constants";
import { hidden } from "ansi-colors";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ""
    };
  }
  email;
  _renderNavbar() {
    return (
      <CustomNavbar
        title="FORGOT PASSWORD"
        titleColor="white"
        hasBorder={false}
        hasBack={true}
      />
    );
  }
  _validate() {
    const { email } = this.state;
    if (_.isEmpty(email)) {
      // email is required
      Util.isRequiredMessage("email");

      return false;
    }
    if (!Util.isEmailValid(email)) {
      // invalid email
      Util.topAlertError(INVALID_EMAIL_ERROR);

      return false;
    }

    return true;
  }
  _formSubmit() {
    if (this._validate()) {
      Util.topAlert("Check your email to reset password");
      Actions.pop();
    }
  }
  _renderForm() {
    const { email } = this.state;
    return (
      <View style={styles.formWraper}>
        <Image
          style={{ alignSelf: "center" }}
          source={Images.lock}
          resizeMode="contain"
          style={{ height: 200, width: 200 }}
        />
        <View style={{ width: "100%", marginTop: 30 }}>
          <Text size="large" type="bold" textAlign="center">
            Forgot your password?
          </Text>

          <Text
            textAlign="center"
            color={Colors.text.secondary}
            style={AppStyles.mTop15}
          >
            Enter your email to receive your password reset instructions
          </Text>
          <AppTextInput
            label="Email"
            ref={ref => (this.email = ref)}
            icon={Images.email}
            value={email}
            onChangeText={email => this.setState({ email })}
            onSubmitEditing={() => this._formSubmit()}
          />
          <GradientButton
            style={AppStyles.mTop30}
            color="white"
            onPress={() => this._formSubmit()}
          >
            SUBMIT
          </GradientButton>
        </View>
      </View>
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <KeyboardAwareScrollView contentContainerStyle={AppStyles.padding20}>
          {this._renderForm()}
        </KeyboardAwareScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(ForgotPassword);
