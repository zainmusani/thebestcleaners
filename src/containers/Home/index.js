// @flow
import _ from "lodash";
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  View,
  Image,
  ImageBackground,
  FlatList,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import { Actions } from "react-native-router-flux";
import { Text, CustomNavbar } from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import { setSelectedTab } from "../../actions/GenerealActions";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { appTexts, HOME_CONTENT, appActiveOpacity } from "../../constants";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    Home.instance = this;
  }
  static onEnter() {
    if (Home.instance) {
      Home.instance._onEnter();
    }
  }
  _onEnter() {
    this.props.setSelectedTab(0);
  }
  static onExit() {
    if (Home.instance) {
      Home.instance._onExit();
    }
  }
  _onExit() {
    console.log("exit");
  }
  _renderWelcome() {
    return (
      <View
        style={[
          AppStyles.alignItemsCenter,
          {
            marginBottom: 15
          }
        ]}
      >
        <View
          style={{
            position: "absolute",
            width: "100%",
            height: 30,
            backgroundColor: Colors.background.primary
          }}
        />
        <View
          style={[
            {
              overflow: "hidden",
              backgroundColor: "white",
              width: "90%",
              height: 220,
              marginTop: 10,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20
            },
            AppStyles.centerInner
          ]}
        >
          <Text type="beyond" size="xxxxLarge">
            Welcome to
          </Text>
          <Text
            size="large"
            textAlign="center"
            style={{ lineHeight: 30, marginTop: 20 }}
            color={Colors.text.orange}
          >
            {appTexts.welcomeTagLineStart + "\n" + appTexts.welcomeTagLineEnd}
          </Text>
        </View>
        <Image
          source={Images.clouds_down}
          resizeMode="cover"
          style={{ width: "90%", marginTop: -5 }}
        />
        <Image
          source={Images.buble_one}
          style={{ position: "absolute", left: 5, top: 80 }}
        />
        <Image
          source={Images.buble_two}
          style={{ position: "absolute", right: 5, top: 60 }}
        />
        <Image
          source={Images.buble_three}
          style={{ position: "absolute", right: 10, top: 200 }}
        />
      </View>
    );
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        leftBtnImage={Images.drawer_icon}
        leftBtnPress={Actions.drawerOpen}
        rightBtnImage={Images.washing_machine}
        title="HOME"
        hasBack={false}
        titleColor="white"
        hasBorder={false}
      />
    );
  }
  _renderButtons() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={HOME_CONTENT}
        keyExtractor={Util.keyExtractor}
        renderItem={this._renderItem}
      />
    );
  }
  onItemPress(index) {
    switch (index) {
      case 0: {
        Actions.jump("price_tab");
        break;
      }
      case 1: {
        Actions.jump("new_order_tab");
        break;
      }
      case 2: {
        Actions.jump("our_services_tab");
        break;
      }
    }
  }
  _renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        activeOpacity={appActiveOpacity}
        onPress={() => this.onItemPress(index)}
        style={[
          AppStyles.margin10,
          {
            backgroundColor: "white",
            borderRadius: 20,
            flexDirection: "row"
          }
        ]}
      >
        <View
          style={[
            {
              margin: 5,
              overflow: "hidden",
              borderRadius: 10
            },
            AppStyles.centerInner
          ]}
        >
          <Image
            source={item.image}
            resizeMode="contain"
            style={{ height: 80, width: 80 }}
          />
        </View>
        <View
          style={{
            marginLeft: 10,
            padding: 15,
            paddingRight: 40
          }}
        >
          <Text size="small">{item.title}</Text>
          <Text
            color={Colors.text.secondary}
            size="xSmall"
            style={{ letterSpacing: 1, lineHeight: 18, marginTop: 5 }}
          >
            {item.details}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        {this._renderWelcome()}
        {this._renderButtons()}
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(Home);
