// @flow
import { StyleSheet } from "react-native";
import { Colors, Metrics, AppStyles } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1
    // backgroundColor: Colors.grey6
    // ...AppStyles.centerInner
  },
  image: {
    width: "100%",
    height: "100%",
    ...AppStyles.flex,
    ...AppStyles.spaceBetween
  },
  topImage: { width: "100%", height: 100 }
});
