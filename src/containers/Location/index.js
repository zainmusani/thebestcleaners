// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, Image } from "react-native";
import styles from "./styles";
import { Text, CustomNavbar, PriceNewOrderList } from "../../components";
import { Images, Colors, AppStyles, Metrics } from "../../theme";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
const DEFAULT_PADDING = { top: 10, right: 10, bottom: 10, left: 10 };
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marker: {
        LatLng: {
          latitude: 25.276987,
          longitude: 55.296249
        },
        title: "Home Place",
        subtitle: "UAE Drive"
      }
    };
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        rightBtnText="DONE"
        title="LOCATION"
        titleColor="white"
        hasBorder={false}
      />
    );
  }

  _onLocationSelect(data, details) {
    let marker = {
      LatLng: {
        latitude: details.geometry.location.lat,
        longitude: details.geometry.location.lng
      },
      title: details.name,
      subtitle: details.formatted_address
    };

    this.setState({ marker }, () => {
      setTimeout(() => {
        this.mapRef.fitToCoordinates([this.state.marker.LatLng], {
          edgePadding: {
            top: 10,
            right: 10,
            bottom: 10,
            left: 10
          },
          animated: true
        });
        // this.mapRef.fitToCoordinates([this.state.marker.LatLng], {
        //   edgePadding: DEFAULT_PADDING,
        //   animated: true
        // });
      }, 1000);
    });
  }

  _renderAutoComplete() {
    const autoCompStyles = {
      textInputContainer: {
        justifyContent: "center",
        alignSelf: "center",
        zindex: 1,
        height: 66,
        backgroundColor: Colors.background.tertiary
      },
      textInput: {
        alignSelf: "center",
        marginTop: 0,
        height: 66,
        color: Colors.text.primary
      },
      container: {
        position: "absolute",
        top: Metrics.navBarHeight,
        right: 0,
        left: 0,
        zIndex: 1
      },
      listView: {
        backgroundColor: Colors.background.tertiary
      }
    };
    return (
      <GooglePlacesAutocomplete
        placeholder="Search Location"
        minLength={2} // minimum length of text to search
        autoFocus={false}
        returnKeyType={"search"} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
        keyboardAppearance={"light"} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
        listViewDisplayed="true" // true/false/undefined
        fetchDetails={true}
        renderDescription={row => row.description} // custom description render
        onPress={(data, details = null) => {
          this._onLocationSelect(data, details);
        }}
        query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
          language: "en" // language of the results
        }}
        styles={autoCompStyles}
        nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
        GoogleReverseGeocodingQuery={
          {
            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          }
        }
        GooglePlacesSearchQuery={
          {
            // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          }
        }
        GooglePlacesDetailsQuery={{
          // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
          fields: "formatted_address"
        }}
        filterReverseGeocodingByTypes={[
          "locality",
          "administrative_area_level_3"
        ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
        // predefinedPlaces={[homePlace, workPlace]}
        debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
        renderLeftButton={() => (
          <View style={[AppStyles.centerInner, AppStyles.mLeft15]}>
            <Image
              source={Images.search_icon}
              style={{ height: 26, width: 26 }}
            />
          </View>
        )}
      />
    );
  }

  render() {
    const { marker } = this.state;
    return (
      <View style={styles.container}>
        {this._renderNavbar()}
        {this._renderAutoComplete()}
        <MapView
          ref={ref => {
            this.mapRef = ref;
          }}
          style={{ flex: 1 }}
          provider={PROVIDER_GOOGLE}
          showsUserLocation={true}
          showsMyLocationButton={true}
          showsScale={true}
          showsCompass={true}
          showsBuildings={true}
          zoomControlEnabled={true}
          region={{
            latitude: 25.276987,
            longitude: 55.296249,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
          }}
        >
          <Marker
            draggable
            coordinate={marker.LatLng}
            title={marker.title}
            description={marker.subtitle}
          />
        </MapView>
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(Login);
