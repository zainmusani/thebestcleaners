// @flow
import _ from "lodash";
import { connect } from "react-redux";
import {
  View,
  Image,
  ScrollView,
  Platform,
  TouchableOpacity,
  StatusBar,
  Keyboard,
  Animated
} from "react-native";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Actions } from "react-native-router-flux";
import {
  INVALID_EMAIL_ERROR,
  INVALID_PASSWORD_ERROR,
  lorenIpsam
} from "../../constants";
import { userSigninRequest } from "../../actions/UserActions";
import {
  Text,
  AppTextInput,
  Loader,
  Button,
  GradientButton
} from "../../components";
import { Images, AppStyles, Colors, Metrics } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { appTexts, appActiveOpacity } from "../../constants";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import * as Animatable from "react-native-animatable";

class Login extends Component {
  static propTypes = {
    userSigninRequest: PropTypes.func.isRequired
  };
  state = {
    errors: {},
    loading: false,
    showClouds: true,
    animation: new Animated.Value(0)
  };
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    Platform.select({
      android: this.setState({ showClouds: false })
    });
  };

  _keyboardDidHide = () => {
    // alert("Keyboard Hidden");
    Platform.select({
      android: this.setState({ showClouds: true })
    });
  };

  email;
  password;

  // emailValue = "chris@yopmail.com";
  // passwordValue = "123456";
  // emailValue = "johd@doe.com";
  // passwordValue = "123456";
  // emailValue = "zain23@yopmail.com";
  // passwordValue = "123456";

  emailValue = "";
  passwordValue = "";
  _onSubmitUserName = () => {
    this.password.focus();
  };

  _onSubmitPassword = () => {
    this.password.blur();
  };

  _onChange = (element, value) => {
    const valueRef = `${element}Value`;
    this[valueRef] = value;

    if (!_.isEmpty(this.state.errors)) {
      this.setState({
        errors: { ...this.state.errors, ...{ [element]: "" } }
      });
    }
  };

  _validateForm() {
    const errors = {};
    if (_.isEmpty(this.emailValue)) {
      // email is required

      errors.email = Util.isRequiredMessage("email");
    }
    if (!Util.isEmailValid(this.emailValue)) {
      // invalid email
      errors.email = INVALID_EMAIL_ERROR;
    }
    if (_.isEmpty(this.passwordValue)) {
      // password is required
      errors.password = Util.isRequiredMessage("password");
    }
    if (!Util.isPasswordValid(this.passwordValue)) {
      // invalid password
      errors.password = INVALID_PASSWORD_ERROR;
    }

    if (!_.isEmpty(errors)) {
      this[_.keys(errors)[0]].focus();
      this.setState({
        errors
      });

      return false;
    }

    return true;
  }

  _onSubmit = () => {
    if (this._validateForm()) {
      this.password.blur();
      this.email.blur();

      const payload = {
        email: this.emailValue,
        password: this.passwordValue,
        device_type: Platform.OS
        // device_token: asd
      };
      Util.showLoader(this);
      this.props.userSigninRequest(payload, data => {});
    }
  };

  renderLogo() {
    return (
      <View style={[AppStyles.centerInner, AppStyles.mTop20]}>
        <Image
          source={Images.welcome_logo}
          resizeMode="contain"
          style={AppStyles.logoImage}
        />
      </View>
    );
  }

  renderText() {
    return (
      <View
        style={[
          AppStyles.centerInner,
          AppStyles.mTop20,
          AppStyles.paddingHorizontalBase
        ]}
      >
        <Text size="large" style={{ textAlign: "center" }}>
          {appTexts.welcomeTagLine}
        </Text>
      </View>
    );
  }

  renderLoginForm() {
    return (
      <View>
        <AppTextInput
          label="Username"
          icon={Images.user_name}
          onSubmitEditing={() => this._onSubmitUserName()}
        />
        <AppTextInput
          label="Password"
          ref={ref => (this.password = ref)}
          icon={Images.password}
          onSubmitEditing={() => Actions.reset("drawerMenu")}
          secureTextEntry={true}
        />
        <TouchableOpacity
          onPress={() => Actions.forgot()}
          activeOpacity={appActiveOpacity}
          style={styles.forgotPassWraper}
        >
          <Text color={Colors.text.secondary}>{appTexts.ForgotPass}</Text>
        </TouchableOpacity>
        <GradientButton
          style={AppStyles.mTop30}
          color="white"
          onPress={() => Actions.reset("drawerMenu")}
        >
          LOGIN
        </GradientButton>
        <Text
          textAlign="center"
          color={Colors.text.secondary}
          style={[AppStyles.mTop35, AppStyles.mBottom15]}
        >
          {appTexts.dontHaveAcc}
        </Text>
        <Button
          background={Colors.transparent}
          onPress={() => Actions.register()}
        >
          REGISTER
        </Button>
        <View style={AppStyles.mTop20}>
          <Text textAlign="center" color={Colors.text.secondary} size="xSmall">
            {"By signing up, you agree to"}
          </Text>
          <Text textAlign="center" size="small" style={AppStyles.mTop5}>
            {"The Best Laundry Service"}
          </Text>

          <View style={styles.tcView}>
            <TouchableOpacity
              activeOpacity={appActiveOpacity}
              onPress={() => {
                Actions.content({
                  title: "Terms & Condition",
                  detail: lorenIpsam
                });
              }}
            >
              <Text
                textAlign="center"
                size="xSmall"
                color={Colors.text.secondary}
              >
                Terms of Conditions
              </Text>
            </TouchableOpacity>
            <Text
              textAlign="center"
              size="xSmall"
              color={Colors.text.secondary}
            >
              {" and "}
            </Text>
            <TouchableOpacity
              activeOpacity={appActiveOpacity}
              onPress={() => {
                Actions.content({
                  title: "Privacy Policy",
                  detail: lorenIpsam
                });
              }}
            >
              <Text
                textAlign="center"
                size="xSmall"
                color={Colors.text.secondary}
              >
                Privacy Policy
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const { errors, loading } = this.state;

    return (
      <View style={AppStyles.flex}>
        <LinearGradient
          colors={Colors.gradients.appBackground}
          start={AppStyles.gradientValues.appBackgroundStart}
          end={AppStyles.gradientValues.appBackgroundEnd}
          style={styles.container}
        >
          <Animatable.Image
            animation={{
              from: { translateY: 0 },
              to: { translateY: -Metrics.screenHeight - 1460 }
            }}
            duration={40000}
            delay={1}
            easing={t => Math.pow(t, 1.7)}
            iterationCount={100}
            iterationCount="infinite"
            useNativeDriver
            source={Images.login_bubbles_more}
            style={{
              width: "100%",
              position: "absolute",
              bottom: -730
            }}
          />

          <KeyboardAwareScrollView
            contentContainerStyle={AppStyles.pBottom100}
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled"
          >
            {this.renderLogo()}
            {this.renderText()}
            {this.renderLoginForm()}
          </KeyboardAwareScrollView>

          <Loader loading={loading} />
        </LinearGradient>

        {this.state.showClouds && (
          <Image
            resizeMode="cover"
            source={Images.welcome_bottom}
            style={{
              width: "100%",
              position: "absolute",
              bottom: 0,
              right: 0,
              left: 0,
              zIndex: 0
            }}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { userSigninRequest };

export default connect(
  mapStateToProps,
  actions
)(Login);
