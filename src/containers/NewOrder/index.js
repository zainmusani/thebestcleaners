// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Text,
  CustomNavbar,
  PriceNewOrderList,
  GradientButton
} from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import ActionSheet from "react-native-actionsheet";
import { setSelectedTab } from "../../actions/GenerealActions";

import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";

class NewOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCheckOut: false
    };
    NewOrder.instance = this;
  }
  static onEnter() {
    if (NewOrder.instance) {
      NewOrder.instance._onEnter();
    }
  }
  _onEnter() {
    this.props.setSelectedTab(2);
  }
  static onExit() {
    if (NewOrder.instance) {
      NewOrder.instance._onExit();
    }
  }
  _onExit() {
    //
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        leftBtnImage={Images.drawer_icon}
        leftBtnPress={Actions.drawerOpen}
        rightBtnImage={Images.washing_machine}
        title="NEW ORDER"
        titleColor="white"
        hasBorder={false}
        hasBack={false}
      />
    );
  }
  showActionSheet(show) {
    this.setState({ showCheckOut: show });
  }
  render() {
    return (
      <View style={styles.container}>
        {this._renderNavbar()}
        <PriceNewOrderList
          showCheckout={show => {
            this.showActionSheet(show);
          }}
        />
        {this.state.showCheckOut && (
          <View
            style={[
              {
                height: 80,
                width: "100%",
                backgroundColor: Colors.white,
                position: "absolute",
                bottom: 0,
                paddingLeft: 10,
                paddingRight: 10
              },
              AppStyles.centerInner
            ]}
          >
            <GradientButton color="white" onPress={() => Actions.check_out()}>
              CHECK OUT
            </GradientButton>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(NewOrder);
