// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, FlatList } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, CustomNavbar, PriceNewOrderList } from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { setSelectedTab } from "../../actions/GenerealActions";
import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";
const Nots = [
  {
    order_no: "12345",
    status: "Active"
  },
  {
    order_no: "12345",
    status: "Active"
  },
  {
    order_no: "12345",
    status: "Active"
  },
  {
    order_no: "12345",
    status: "Active"
  },
  {
    order_no: "12345",
    status: "Active"
  },
  {
    order_no: "12345",
    status: "Active"
  },
  {
    order_no: "12345",
    status: "Active"
  },
  {
    order_no: "12345",
    status: "Active"
  }
];
class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    Notification.instance = this;
  }
  static onEnter() {
    if (Notification.instance) {
      Notification.instance._onEnter();
    }
  }
  _onEnter() {
    this.props.setSelectedTab(4);
  }
  static onExit() {
    if (Notification.instance) {
      Notification.instance._onExit();
    }
  }
  _onExit() {
    //
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        leftBtnImage={Images.drawer_icon}
        leftBtnPress={Actions.drawerOpen}
        rightBtnImage={Images.washing_machine}
        title="ORDER HISTORY"
        titleColor="white"
        hasBorder={false}
        hasBack={false}
      />
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}

        <FlatList
          showsVerticalScrollIndicator={false}
          data={Nots}
          renderItem={({ item }) => {
            return (
              <View style={styles.itemWraper}>
                <View style={styles.textRow}>
                  <Text>{"Order No "}</Text>
                  <Text>{item.order_no}</Text>
                </View>
                <View style={styles.textRow}>
                  <Text>{"Order Status "}</Text>
                  <Text>{item.status}</Text>
                </View>
              </View>
            );
          }}
          keyExtractor={Util.keyExtractor}
        />
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(Notification);
