// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  View,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, CustomNavbar, PriceNewOrderList } from "../../components";
import { Images, Colors, AppStyles, Metrics } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { setSelectedTab } from "../../actions/GenerealActions";

import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";
class OurServices extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    OurServices.instance = this;
  }
  static onEnter() {
    if (OurServices.instance) {
      OurServices.instance._onEnter();
    }
  }
  _onEnter() {
    this.props.setSelectedTab(3);
  }
  static onExit() {
    if (OurServices.instance) {
      OurServices.instance._onExit();
    }
  }
  _onExit() {
    //
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        leftBtnImage={Images.drawer_icon}
        leftBtnPress={Actions.drawerOpen}
        rightBtnImage={Images.washing_machine}
        title="OUR SERVICES"
        titleColor="white"
        hasBorder={false}
        hasBack={false}
      />
    );
  }
  _renderItem({ item, index }) {
    return (index + 1) % 3 === 0 ? (
      <View style={styles.bigItemWraper}>
        <View style={[AppStyles.flexRow, AppStyles.flex]}>
          <Image source={item.image} />
          <View style={styles.textParent}>
            <Text>{item.title} </Text>
            <Text
              color={Colors.text.secondary}
              size="xxxSmall"
              style={AppStyles.mTop10}
            >
              {item.description}
            </Text>
          </View>
        </View>
      </View>
    ) : (
      <View style={styles.smallItemWraper}>
        <Image source={item.image} />
        <Text>{item.title} </Text>
        <Text
          color={Colors.text.secondary}
          size="xxxSmall"
          style={AppStyles.mTop10}
        >
          {item.description}
        </Text>
      </View>
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <ScrollView>
          <View
            style={{
              flexWrap: "wrap",
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              paddingLeft: 15,
              paddingRight: 15,
              marginBottom: 20
            }}
          >
            {/* <FlatList
            data={this.props.our_services}
            renderItem={this._renderItem}
            keyExtractor={Util.keyExtractor}
          /> */}
            {this.props.our_services.map((item, index) => {
              return (index + 1) % 3 === 0 ? (
                <TouchableOpacity
                  activeOpacity={appActiveOpacity}
                  style={styles.bigItemWraper}
                  onPress={() => Actions.service_detail({ item: item })}
                >
                  <View style={[AppStyles.flexRow, AppStyles.flex]}>
                    <Image source={item.image} />
                    <View style={styles.textParent}>
                      <Text>{item.title} </Text>
                      <Text
                        color={Colors.text.secondary}
                        size="xxxSmall"
                        style={AppStyles.mTop10}
                      >
                        {item.description}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  activeOpacity={appActiveOpacity}
                  style={styles.smallItemWraper}
                  onPress={() => Actions.service_detail({ item: item })}
                >
                  <Image
                    source={item.image}
                    resizeMode="contain"
                    style={{
                      height: (Metrics.screenWidth / 2 - 28) * 0.79,
                      width: Metrics.screenWidth / 2 - 28
                    }}
                  />
                  <Text style={AppStyles.mTop10}>{item.title} </Text>
                  <Text
                    color={Colors.text.secondary}
                    size="xxxSmall"
                    style={AppStyles.mTop5}
                  >
                    {item.description}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = ({ general }) => ({
  our_services: general.our_services
});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(OurServices);
