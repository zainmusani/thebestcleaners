// @flow
import { StyleSheet } from "react-native";
import { Colors, Metrics } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  bigItemWraper: {
    marginTop: 15,
    padding: 5,
    backgroundColor: "white",
    borderRadius: 10,
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    width: "100%",

    elevation: 1,
    overflow: "hidden"
  },
  smallItemWraper: {
    padding: 5,
    paddingBottom: 15,
    marginTop: 15,
    width: Metrics.screenWidth / 2 - 23,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: "white",
    borderRadius: 17,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1,
    overflow: "hidden",
    alignItems: "center"
  },
  textParent: {
    paddingTop: 40,
    paddingLeft: 20,
    flex: 1
  }
});
