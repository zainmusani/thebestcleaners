// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, CustomNavbar, PriceNewOrderList } from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { setSelectedTab } from "../../actions/GenerealActions";

import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";

class PriceList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    PriceList.instance = this;
  }
  static onEnter() {
    if (PriceList.instance) {
      PriceList.instance._onEnter();
    }
  }
  _onEnter() {
    this.props.setSelectedTab(1);
  }
  static onExit() {
    if (PriceList.instance) {
      PriceList.instance._onExit();
    }
  }
  _onExit() {
    console.log("exit");
  }
  _renderNavbar() {
    return (
      <CustomNavbar
        leftBtnImage={Images.drawer_icon}
        leftBtnPress={Actions.drawerOpen}
        rightBtnImage={Images.washing_machine}
        title="PRICE DETAILS"
        titleColor="white"
        hasBorder={false}
        hasBack={false}
      />
    );
  }
  render() {
    return (
      <View style={styles.container}>
        {this._renderNavbar()}
        <PriceNewOrderList isPriceList={true} />
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(PriceList);
