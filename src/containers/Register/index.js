// @flow
import _ from "lodash";
import { connect } from "react-redux";
import {
  View,
  Image,
  ScrollView,
  Platform,
  TouchableOpacity,
  StatusBar
} from "react-native";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Actions } from "react-native-router-flux";
import { INVALID_EMAIL_ERROR, INVALID_PASSWORD_ERROR } from "../../constants";
import { userSigninRequest } from "../../actions/UserActions";
import {
  Text,
  AppTextInput,
  Loader,
  Button,
  GradientButton
} from "../../components";
import { Images, AppStyles, Colors } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { appTexts, appActiveOpacity } from "../../constants";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class Register extends Component {
  static propTypes = {
    userSigninRequest: PropTypes.func.isRequired
  };
  state = {
    errors: {},
    loading: false
  };
  phone;
  email;
  password;
  conPassword;
  address;

  // emailValue = "chris@yopmail.com";
  // passwordValue = "123456";
  // emailValue = "johd@doe.com";
  // passwordValue = "123456";
  // emailValue = "zain23@yopmail.com";
  // passwordValue = "123456";

  emailValue = "";
  passwordValue = "";

  _onSubmitName = () => {
    this.phone.focus();
  };
  _onSubmitPhone = () => {
    this.email.focus();
  };

  _onSubmitEmail = () => {
    this.password.focus();
  };

  _onSubmitpassword = () => {
    this.password.blur();
    this.conPassword.focus();
  };
  _onSubmitconPassword = () => {
    this.conPassword.blur();
    this.address.focus();
  };

  _onChange = (element, value) => {
    const valueRef = `${element}Value`;
    this[valueRef] = value;

    if (!_.isEmpty(this.state.errors)) {
      this.setState({
        errors: { ...this.state.errors, ...{ [element]: "" } }
      });
    }
  };

  _validateForm() {
    const errors = {};
    if (_.isEmpty(this.emailValue)) {
      // email is required

      errors.email = Util.isRequiredMessage("email");
    }
    if (!Util.isEmailValid(this.emailValue)) {
      // invalid email
      errors.email = INVALID_EMAIL_ERROR;
    }
    if (_.isEmpty(this.passwordValue)) {
      // password is required
      errors.password = Util.isRequiredMessage("password");
    }
    if (!Util.isPasswordValid(this.passwordValue)) {
      // invalid password
      errors.password = INVALID_PASSWORD_ERROR;
    }

    if (!_.isEmpty(errors)) {
      this[_.keys(errors)[0]].focus();
      this.setState({
        errors
      });

      return false;
    }

    return true;
  }

  _onSubmit = () => {
    if (this._validateForm()) {
      this.password.blur();
      this.email.blur();

      const payload = {
        email: this.emailValue,
        password: this.passwordValue,
        device_type: Platform.OS
        // device_token: asd
      };
      Util.showLoader(this);
      this.props.userSigninRequest(payload, data => {});
    }
  };

  renderLogo() {
    return (
      <View style={[AppStyles.centerInner, AppStyles.mTop20]}>
        <Image
          source={Images.welcome_logo}
          resizeMode="contain"
          style={AppStyles.logoImage}
        />
      </View>
    );
  }

  renderText() {
    return (
      <View
        style={[
          AppStyles.centerInner,
          AppStyles.mTop20,
          AppStyles.paddingHorizontalBase
        ]}
      >
        <Text size="large" style={{ textAlign: "center" }}>
          {"Create Account"}
        </Text>
        <View style={AppStyles.flexRow}>
          <Text size="large">{"Already have an account? "}</Text>
          <TouchableOpacity
            activeOpacity={appActiveOpacity}
            onPress={() => Actions.pop()}
          >
            <Text
              size="large"
              style={{
                textDecorationLine: "underline"
              }}
              color={Colors.text.secondary}
            >
              Log In
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderLoginForm() {
    return (
      <View style={{ paddingBottom: 40 }}>
        <AppTextInput
          label="Name"
          ref={ref => {
            this.email = ref;
          }}
          icon={Images.user_name}
          onSubmitEditing={() => this._onSubmitName()}
        />
        <AppTextInput
          label="Phone Number"
          ref={ref => {
            this.phone = ref;
          }}
          icon={Images.phone_call}
          onSubmitEditing={() => this._onSubmitPhone()}
          keyboardType="phone-pad"
        />
        <AppTextInput
          label="Email"
          ref={ref => (this.email = ref)}
          icon={Images.email}
          onSubmitEditing={() => this._onSubmitEmail()}
          keyboardType="email-address"
        />
        <AppTextInput
          label="Password"
          icon={Images.password}
          onSubmitEditing={() => this._onSubmitpassword()}
          ref={ref => (this.password = ref)}
          secureTextEntry={true}
        />
        <AppTextInput
          label="Confirm Password"
          icon={Images.password}
          ref={ref => (this.conPassword = ref)}
          secureTextEntry={true}
        />
        <AppTextInput
          label="Location"
          icon={Images.location}
          secureTextEntry={true}
        />

        <AppTextInput
          label="Address"
          ref={ref => (this.address = ref)}
          icon={Images.address}
          secureTextEntry={false}
        />

        <GradientButton
          style={AppStyles.mTop30}
          color="white"
          onPress={() => Actions.dashboard()}
        >
          CONTINUE
        </GradientButton>
      </View>
    );
  }

  render() {
    const { errors, loading } = this.state;

    return (
      <View style={AppStyles.flex}>
        <LinearGradient
          colors={Colors.gradients.appBackground}
          start={AppStyles.gradientValues.appBackgroundStart}
          end={AppStyles.gradientValues.appBackgroundEnd}
          style={styles.container}
        >
          <KeyboardAwareScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled"
          >
            {this.renderLogo()}
            {this.renderText()}
            {this.renderLoginForm()}
          </KeyboardAwareScrollView>

          <Loader loading={loading} />
        </LinearGradient>

        <Image
          resizeMode="cover"
          source={Images.welcome_bottom}
          style={{
            width: "100%",
            position: "absolute",
            bottom: 0,
            right: 0,
            left: 0,
            zIndex: 0
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { userSigninRequest };

export default connect(
  mapStateToProps,
  actions
)(Register);
