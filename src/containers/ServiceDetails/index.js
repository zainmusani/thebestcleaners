// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, Image, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text, CustomNavbar, PriceNewOrderList } from "../../components";
import { Images, Colors, AppStyles } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import PropTypes from "prop-types";

import LinearGradient from "react-native-linear-gradient";
import { setSelectedTab } from "../../actions/GenerealActions";

import {
  appTexts /* HOME_CONTENT */,
  appActiveOpacity,
  lorenIpsam
} from "../../constants";

class ServiceDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  static propTypes = {
    item: PropTypes.object.isRequired
  };

  static defaultProps = {};
  _renderNavbar() {
    return (
      <CustomNavbar
        title={this.props.item.title.toUpperCase()}
        titleColor="white"
        hasBorder={false}
        hasBack={true}
      />
    );
  }
  render() {
    const { item } = this.props;
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <ScrollView contentContainerStyle={AppStyles.basePadding}>
          <View style={AppStyles.boxStyle}>
            <Image
              source={Images.service_detail}
              resizeMode="cover"
              style={{ height: 220, width: 350 }}
            />

            <Text style={AppStyles.mTop20}>{lorenIpsam}</Text>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(ServiceDetails);
