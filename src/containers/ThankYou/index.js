// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, ScrollView, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import { setSelectedTab } from "../../actions/GenerealActions";

import {
  Text,
  CustomNavbar,
  PriceNewOrderList,
  GradientButton
} from "../../components";
import { Images, Colors, AppStyles, Metrics } from "../../theme";
import styles from "./styles";
import Util from "../../util";
import LinearGradient from "react-native-linear-gradient";
import { appTexts /* HOME_CONTENT */, appActiveOpacity } from "../../constants";

class ThankYou extends Component {
  _renderNavbar() {
    return (
      <CustomNavbar
        title="NEW ORDER"
        titleColor="white"
        hasBorder={false}
        hasBack={false}
      />
    );
  }
  onDonePress = () => {
    Actions.reset("drawerMenu");
  };
  _renderDone() {
    return (
      <GradientButton
        style={{ margin: 20, marginBottom: 0 }}
        color="white"
        onPress={() => this.onDonePress()}
      >
        Done
      </GradientButton>
    );
  }
  render() {
    return (
      <LinearGradient
        colors={Colors.gradients.appBackground}
        start={AppStyles.gradientValues.appBackgroundStart}
        end={AppStyles.gradientValues.appBackgroundEnd}
        style={styles.container}
      >
        {this._renderNavbar()}
        <ScrollView
          contentContainerStyle={{
            flex: 1,

            padding: Metrics.baseMargin
          }}
        >
          <View style={styles.itemWraper}>
            <Text type="base3" size="xLarge">
              THANK YOU
            </Text>
            <Image style={AppStyles.margin30} source={Images.thankYou} />
            <Text color={Colors.text.orange} size="large">
              Order #4568 is confirmed
            </Text>

            <Text
              style={{ lineHeight: 20, textAlign: "center", marginTop: 30 }}
              color={Colors.text.secondary}
              size="xxSmall"
            >
              {
                "Lorem ipsum dolor sit amet, consecr adipcin gelit do eiusmod tempor incididunt."
              }
            </Text>
            {this._renderDone()}
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = () => ({});

const actions = { setSelectedTab };

export default connect(
  mapStateToProps,
  actions
)(ThankYou);
