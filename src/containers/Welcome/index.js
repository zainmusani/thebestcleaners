// @flow
import _ from "lodash";
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, Image, ImageBackground } from "react-native";
import PropTypes from "prop-types";
import { Actions } from "react-native-router-flux";
import { Text } from "../../components";
import { Images, Colors } from "../../theme";
import styles from "./styles";
import Util from "../../util";

class Welcome extends Component {
  static propTypes = {
    userData: PropTypes.object.isRequired
  };

  componentDidMount() {
    const { userData } = this.props;

    setTimeout(() => {
      if (!_.isEmpty(userData) && !_.isEmpty(userData.access_token)) {
        Actions.reset("dashboard");
      } else {
        Actions.reset("login");
      }
    }, 2000);
  }

  _renderTop() {
    return (
      <Image
        resizeMode="contain"
        source={Images.welcome_top}
        style={styles.welcomeTop}
      />
    );
  }

  _renderLogo() {
    return (
      <Image
        resizeMode="contain"
        source={Images.welcome_logo}
        style={styles.logo}
      />
    );
  }

  _renderMiddle() {
    return (
      <Image
        resizeMode="contain"
        source={Images.welcome_mid}
        style={styles.welcomeMid}
      />
    );
  }

  _renderBottom() {
    return (
      <Image
        resizeMode="stretch"
        source={Images.welcome_bottom}
        style={styles.welcomeBottom}
      />
    );
  }

  _renderBubbles() {
    return (
      <Image
        resizeMode="contain"
        source={Images.welcome_bubble}
        style={styles.welcomeBubble}
      />
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={Images.welcome_background}
          style={styles.backgroundImage}
        >
          {this._renderBubbles()}
          {this._renderTop()}
          {this._renderLogo()}
          {this._renderMiddle()}
          {this._renderBottom()}
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  userData: user.data
});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(Welcome);
