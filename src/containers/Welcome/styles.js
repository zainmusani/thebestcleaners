// @flow
import { StyleSheet } from "react-native";
import { Colors, Metrics, AppStyles } from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  welcomeTop: {
    // maxHeight: "20%"
    maxWidth: "100%"
  },
  logo: {
    marginVertical: "5%",
    maxHeight: "19%"
  },
  welcomeMid: {
    maxHeight: "37.5%"
  },
  welcomeBottom: {
    width: "100%"
    // maxHeight: "7%"
  },
  welcomeBubble: {
    position: "absolute",
    bottom: "15%"
  }
});
