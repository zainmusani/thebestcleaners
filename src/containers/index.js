// @flow
import Login from "./Login";
import Welcome from "./Welcome";
import Register from "./Register";
import Home from "./Home";
import PriceList from "./PriceList";
import NewOrder from "./NewOrder";
import Location from "./Location";
import OurServices from "./OurServices";
import Notifications from "./Notifications";
import CkeckOut from "./CkeckOut";
import ThankYou from "./ThankYou";
import DateTimePick from "./DateTimePick";
import AddAddress from "./AddAddress";
import DeliveryAddress from "./DeliveryAddress";
import Content from "./Content";
import ForgotPassword from "./ForgotPassword";
import Contact from "./Contact";
import ServiceDetails from "./ServiceDetails";

export {
  Login,
  Welcome,
  Register,
  Home,
  PriceList,
  NewOrder,
  Location,
  OurServices,
  Notifications,
  CkeckOut,
  ThankYou,
  DateTimePick,
  AddAddress,
  DeliveryAddress,
  Content,
  ForgotPassword,
  Contact,
  ServiceDetails
};
