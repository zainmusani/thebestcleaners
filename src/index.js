// @flow
import React, { Component } from "react";
import { Provider } from "react-redux";
import { AppRegistry, View, StatusBar } from "react-native";
import { MessageBar } from "./components";
import configureStore from "./store";
import AppNavigator from "./navigator";
import applyConfigSettings from "./config";
import AppStyles from "./theme/AppStyles";
import { Colors } from "./theme";

const reducers = require("./reducers").default;

applyConfigSettings();

export default class App extends Component {
  state = {
    isLoading: true,
    store: configureStore(reducers, () => {
      this._loadingCompleted();
      this.setState({ isLoading: false });
    })
  };

  _loadingCompleted() {
    //  DataHandler.setStore(this.state.store);
  }

  componentDidMount() {}

  render() {
    if (this.state.isLoading) {
      return null;
    }

    return (
      <View style={AppStyles.flex}>
        <StatusBar
          backgroundColor={Colors.background.primary}
          barStyle="light-content"
        />
        <Provider store={this.state.store}>
          <AppNavigator />
        </Provider>
        <MessageBar />
      </View>
    );
  }
}

AppRegistry.registerComponent("AutoConnect", () => App);
