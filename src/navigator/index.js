// @flow
import React from "react";
import { connect } from "react-redux";
import {
  Stack,
  Scene,
  Router,
  Actions,
  Tabs,
  Drawer
} from "react-native-router-flux";

import styles from "./styles";
import { Colors, Images } from "../theme";
import { TabIcon, Tabbar, SideMenu } from "../components";
import {
  Login,
  Welcome,
  Register,
  Home,
  PriceList,
  NewOrder,
  Location,
  OurServices,
  Notifications,
  CkeckOut,
  ThankYou,
  DateTimePick,
  AddAddress,
  DeliveryAddress,
  Content,
  ForgotPassword,
  Contact,
  ServiceDetails
} from "../containers";
import Util from "../util";

function onBackPress() {
  if (Actions.state.index === 0) {
    return false;
  }
  Actions.pop();
  return true;
}

const navigator = Actions.create(
  <Stack
    key="root"
    titleStyle={styles.title}
    headerStyle={styles.header}
    headerTintColor={Colors.navbar.text}
    hideNavBar
  >
    <Scene key="welcome" component={Welcome} hideNavBar initail />
    <Drawer
      key="drawerMenu"
      contentComponent={SideMenu}
      drawerWidth={250}
      drawerPosition="left"
      headerStyle={styles.header}
      drawerImage={Images.drawer_icon}
    >
      <Scene key="dashboard" hideNavBar>
        <Tabs
          key="tabbar"
          swipeEnabled={false}
          tabBarPosition="bottom"
          tabBarComponent={() => <Tabbar />}
          lazy={!Util.isPlatformAndroid()}
          hideNavBar
        >
          <Stack key="home_tab" title="Home" icon={TabIcon}>
            <Scene key="home_tab" component={Home} hideNavBar />
          </Stack>
          <Stack key="price_tab" title="Price" icon={TabIcon}>
            <Scene key="price_tab" component={PriceList} hideNavBar />
          </Stack>
          <Stack key="new_order_tab" title="Order" icon={TabIcon}>
            <Scene key="new_order_tab" component={NewOrder} hideNavBar />
          </Stack>
          <Stack
            key="our_services_tab"
            title="Services"
            icon={TabIcon}
            hideNavBar
          >
            <Scene
              key="our_services_tab"
              component={OurServices}
              title="Calendar"
            />
          </Stack>
          <Stack
            key="notification_tab"
            title="Notification"
            icon={TabIcon}
            hideNavBar
          >
            <Scene key="notification_tab" component={Notifications} />
          </Stack>
        </Tabs>
      </Scene>
    </Drawer>
    <Scene key="login" component={Login} hideNavBarv />
    <Scene key="register" component={Register} hideNavBar />
    <Scene key="plist" component={PriceList} hideNavBar />
    <Scene key="location" component={Location} hideNavBar />
    <Scene key="check_out" component={CkeckOut} hideNavBar />
    <Scene key="thank_you" component={ThankYou} hideNavBar />
    <Scene key="dateTimePicker" component={DateTimePick} hideNavBar />
    <Scene key="address" component={AddAddress} hideNavBarv />
    <Scene key="delivery" component={DeliveryAddress} hideNavBarv />
    <Scene key="content" component={Content} hideNavBarv />
    <Scene key="forgot" component={ForgotPassword} hideNavBar />
    <Scene key="contact" component={Contact} hideNavBar />
    <Scene key="service_detail" component={ServiceDetails} hideNavBar />
  </Stack>
);

export default () => (
  <AppNavigator navigator={navigator} backAndroidHandler={onBackPress} />
);

const AppNavigator = connect()(Router);
