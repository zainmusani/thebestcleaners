import Immutable from "seamless-immutable";
import { SET_SELECTED_TAB } from "../actions/ActionTypes";
import { Images } from "../theme";

const initialState = Immutable({
  selectedIndex: 0,
  priceCatogiries: {
    catogories: [
      {
        title: "Clothes",
        data: [
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          },
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          },
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          },
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          },
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          },
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          },
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          },
          {
            name: "Shirt",
            image: Images.shirt,
            price_iron: "10",
            price_was_iron: "15"
          }
        ]
      },
      {
        title: "Shoes",
        data: [
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          },
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          },
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          },
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          },
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          },
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          },
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          },
          {
            name: "Shose",
            image: Images.shirt,
            price_iron: "13",
            price_was_iron: "18"
          }
        ]
      },
      {
        title: "UPHOLSTERY",
        data: [
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          },
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          },
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          },
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          },
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          },
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          },
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          },
          {
            name: "Upholstery",
            image: Images.shirt,
            price_iron: "50",
            price_was_iron: "70"
          }
        ]
      }
    ]
  },
  our_services: [
    {
      title: "Wash Only",
      image: Images.wash_only,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    },
    {
      title: "Iron Only",
      image: Images.iron_only,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    },
    {
      title: "Wash & Iron",
      image: Images.wash_iron,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    },
    {
      title: "Dry Wash",
      image: Images.dry_wash,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    },
    {
      title: "Chemical Wash",
      image: Images.chemical_wash,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    },
    {
      title: "Double Wash",
      image: Images.wash_iron,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    },
    {
      title: "Simple Wash",
      image: Images.iron_only,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    },
    {
      title: "Steam Wash",
      image: Images.dry_wash,
      description: "Lorem ipsum dolor amet, consectetur adipiscing."
    }
  ]
});
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_SELECTED_TAB: {
      return Immutable.merge(state, {
        selectedIndex: action.selectedIndex
      });
    }
    default:
      return state;
  }
};
