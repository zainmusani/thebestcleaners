import { combineReducers } from "redux";

import navigator from "./navigator";
import user from "./user";
import general from "./genereal";

export default combineReducers({
  route: navigator,
  user,
  general
});
