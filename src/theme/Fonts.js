// @flow

import { Platform } from "react-native";

const type = {
  base: "CirceRounded-Regular",
  base3: "CirceRounded-Regular3",
  base4: "CirceRounded-Regular4",
  beyond: Platform.select({
    ios: "BeyondTheMountains",
    android: "beyond-the-mountains"
  })
  /* base: Platform.select({
    ios: "Helvetica Neue",
    android: "Roboto"
  }),
  bold: Platform.select({
    ios: "HelveticaNeue-Medium",
    android: "Roboto"
  }),
  italic: Platform.select({
    ios: "HelveticaNeue-Italic",
    android: "Roboto"
  }) */
};

// Metrics.generatedFontSize(ios, android)

const size = {
  xxxxSmall: 10,
  xxxSmall: 11,
  xxSmall: 13,
  xSmall: 14,
  small: 15,
  normal: 17,
  medium: 18,
  large: 20,
  xLarge: 24,
  xxLarge: 30,
  xxxLarge: 36,
  xxxxLarge: 40
};

export default {
  type,
  size
};
