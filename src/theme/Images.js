const logo = require("../assets/images/logo.png");
const home = require("../assets/icons/home_tab/home_tab.png");
const order = require("../assets/icons/new_order_tab/new_order_tab.png");
const notification = require("../assets/icons/notification_tab/notification_tab.png");
const services = require("../assets/icons/our_services_tab/our_services_tab.png");
const price = require("../assets/icons/price_tab/price_tab.png");
const welcome_background = require("../assets/images/welcome_background/welcome_background.png");
const welcome_bottom = require("../assets/images/welcome_bottom/welcome_bottom.png");
const welcome_logo = require("../assets/images/welcome_logo/welcome_logo.png");
const welcome_mid = require("../assets/images/welcome_mid/welcome_mid.png");
const welcome_top = require("../assets/images/welcome_top/welcome_top.png");
const welcome_bubble = require("../assets/images/welcome_bubble/welcome_bubble.png");
const user_name = require("../assets/icons/user_name/user_name.png");
const password = require("../assets/icons/password/password.png");
const address = require("../assets/icons/address/address.png");
const email = require("../assets/icons/email/email.png");
const location = require("../assets/icons/location/location.png");
const phone_call = require("../assets/icons/phone_call/phone_call.png");
const arrow_right = require("../assets/icons/arrow_right/arrow_right.png");
const order_history_drawer = require("../assets/icons/order_history_drawer/order_history_drawer.png");
const about_us_drawer = require("../assets/icons/about_us_drawer/about_us_drawer.png");
const privacy_policy_drawer = require("../assets/icons/privacy_policy_drawer/privacy_policy_drawer.png");
const terms_drawer = require("../assets/icons/terms_drawer/terms_drawer.png");
const contact_drawer = require("../assets/icons/contact_drawer/contact_drawer.png");
const logout_drawer = require("../assets/icons/logout_drawer/logout_drawer.png");
const drawer_icon = require("../assets/icons/drawer_icon/drawer_icon.png");
const home_box = require("../assets/images/home_box/home_box.png");
const washing_machine = require("../assets/icons/washing_machine/washing_machine.png");
const clouds_down = require("../assets/images/clouds_down/clouds_down.png");
const buble_one = require("../assets/icons/buble_one/buble_one.png");
const buble_two = require("../assets/icons/buble_two/buble_two.png");
const buble_three = require("../assets/icons/buble_three/buble_three.png");

const home_ledger = require("../assets/images/home_ledger/home_ledger.png");
const home_machine = require("../assets/images/home_machine/home_machine.png");
const home_suites = require("../assets/images/home_suites/home_suites.png");

const cloths_tab = require("../assets/icons/cloths_tab/cloths_tab.png");
const shoes_tab = require("../assets/icons/shoes_tab/shoes_tab.png");
const sofa_tab = require("../assets/icons/sofa_tab/sofa_tab.png");

const cloths_tab_blue = require("../assets/icons/cloths_tab/cloth_tab_blue.png");
const shoes_tab_blue = require("../assets/icons/shoes_tab/shoes_tab_blue.png");
const sofa_tab_blue = require("../assets/icons/sofa_tab/sofa_tab_blue.png");

const shirt = require("../assets/images/shirt/shirt.png");

const sub = require("../assets/icons/sub/sub.png");
const add = require("../assets/icons/add/add.png");

const back_btn = require("../assets/icons/back_btn/back_btn.png");
const search_icon = require("../assets/icons/search_icon/search_icon.png");

const chemical_wash = require("../assets/images/chemical_wash/chemical_wash.png");
const dry_wash = require("../assets/images/dry_wash/dry_wash.png");
const wash_only = require("../assets/images/wash_only/wash_only.png");
const wash_iron = require("../assets/images/wash_iron/wash_iron.png");
const iron_only = require("../assets/images/iron_only/iron_only.png");

const check_icon = require("../assets/icons/check_icon/check_icon.png");
const notepad_icon = require("../assets/icons/notepad_icon/notepad_icon.png");
const credit_card_icon = require("../assets/icons/credit_card_icon/credit_card_icon.png");
const checkOut_location_icon = require("../assets/icons/checkOut_location_icon/checkOut_location_icon.png");
const clock_icon = require("../assets/icons/clock_icon/clock_icon.png");

const checked_icon = require("../assets/icons/checked_icon/checked_icon.png");
const unckeck_icon = require("../assets/icons/unckeck_icon/unckeck_icon.png");

const thankYou = require("../assets/images/thankYou/thankYou.png");

const close_icon = require("../assets/icons/close_icon/close_icon.png");

const arrow_down = require("../assets/icons/arrow_down/arrow_down.png");

const card_selection_icon = require("../assets/icons/card_selection_icon/card_selection_icon.png");
const cash_icon = require("../assets/icons/cash_icon/cash_icon.png");

const add_nav_bar_icon = require("../assets/icons/add_nav_bar_icon/add_nav_bar_icon.png");

const home_blue = require("../assets/icons/home_blue/home_blue.png");

const lock = require("../assets/icons/lock/lock.png");

const contacts_us = require("../assets/icons/contacts_us/contacts_us.png");

const service_detail = require("../assets/images/service_detail/service_detail.png");

const login_bubbles = require("../assets/images/login_bubbles/login_bubbles.png");

const login_bubbles_more = require("../assets/images/login_bubbles/login_bubbles_more.png");

export default {
  logo,
  price,
  order,
  notification,
  services,
  home,
  welcome_background,
  welcome_bottom,
  welcome_logo,
  welcome_mid,
  welcome_top,
  welcome_bubble,
  user_name,
  password,
  address,
  email,
  location,
  phone_call,
  arrow_right,
  order_history_drawer,
  about_us_drawer,
  privacy_policy_drawer,
  terms_drawer,
  contact_drawer,
  logout_drawer,
  drawer_icon,
  home_box,
  washing_machine,
  clouds_down,
  buble_one,
  buble_two,
  buble_three,
  home_ledger,
  home_machine,
  home_suites,
  cloths_tab,
  shoes_tab,
  sofa_tab,
  cloths_tab_blue,
  shoes_tab_blue,
  sofa_tab_blue,
  shirt,
  sub,
  add,
  back_btn,
  search_icon,
  chemical_wash,
  dry_wash,
  wash_only,
  wash_iron,
  iron_only,
  check_icon,
  notepad_icon,
  credit_card_icon,
  checkOut_location_icon,
  clock_icon,
  checked_icon,
  unckeck_icon,
  thankYou,
  close_icon,
  arrow_down,
  card_selection_icon,
  cash_icon,
  add_nav_bar_icon,
  home_blue,
  lock,
  contacts_us,
  service_detail,
  login_bubbles,
  login_bubbles_more
};
